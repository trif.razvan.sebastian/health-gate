"use client";

import { createTheme } from "@mantine/core";
import { Poppins } from 'next/font/google'
const poppins = Poppins({
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
  subsets: ["latin", "latin-ext", "devanagari"],
  style: ["normal", "italic"],
  
});

export const theme = createTheme({
  /* Put your mantine theme override here */
  fontFamily: poppins.className,
  headings: {
    fontFamily: `${poppins.className}, sans-serif`
  }
});
