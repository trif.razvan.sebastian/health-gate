import {
  ActionIcon,
  Burger,
  Flex,
  Indicator,
  Popover,
  ScrollArea,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import LocalMallRoundedIcon from "@mui/icons-material/LocalMallRounded";
import PersonRoundedIcon from "@mui/icons-material/PersonRounded";
import Link from "next/link";
import useGetCart from "../hooks/firebase/useGetCart";
import useGetUserData from "../hooks/firebase/useGetUserData";
import classes from "../modules/NavigationLayout.module.css";
import calculateTransport from "../utils/calculateTransport";
import CartCheckoutButton from "./CartCheckoutButton";
import CartProductsList from "./CartProductsList";

const navigationRoutes = [
  { label: "Abonamente", route: "/subscriptions" },
  { label: "Meniu", route: "/menu" },
  { label: "Cum comand", route: "/how-it-works" },
];

export default function Navbar() {
  const [opened, { toggle }] = useDisclosure();
  const { realtimeCart } = useGetCart();
  const { userData } = useGetUserData();
  const cartItems = realtimeCart.map((prod) => ({
    price: prod.productPrice * 100,
    quantity: prod.productQuantity,
    title: prod.productTitle,
    image: prod.productImage,
    id: prod.productId,
    cartId: prod.id as string,
    menuItems: prod.menuItems as string[],
  }));

  return (
    <div className={classes.headerFlex}>
      <Link href="/">
        <img
          src="/healthgate_logo.svg"
          style={{ width: "10rem" }}
          alt="health gate logo"
        />
      </Link>
      <div className={classes.linksFlex}>
        {navigationRoutes.map((nav, i) => (
          <Link
            key={i}
            style={{
              fontSize: "1rem",
              lineHeight: "150%",
              color: "var(--dark)",
            }}
            href={nav.route}
          >
            {nav.label}
          </Link>
        ))}
      </div>
      <Flex align="flex-start" gap="1.75rem">
        <Popover radius={10} width={400} position="bottom" shadow="md">
          <Popover.Target>
            <ActionIcon w="auto" variant="transparent">
              <Indicator
                size={15}
                inline
                label={realtimeCart?.length}
                color="var(--primary-color)"
                disabled={realtimeCart?.length ? false : true}
              >
                <LocalMallRoundedIcon className="icon" />
              </Indicator>
            </ActionIcon>
          </Popover.Target>
          <Popover.Dropdown style={{ boxShadow: "var(--primary-shadow)" }}>
            <ScrollArea
              mah={300}
              h={realtimeCart.length > 4 ? 300 : "auto"}
              type="hover"
              offsetScrollbars
            >
              <CartProductsList realtimeCart={realtimeCart} />
            </ScrollArea>
            <Flex mt="1rem" align="center" justify="space-between">
              <CartCheckoutButton />
              <div>
                <p className="p-text">Total:</p>
                <h4 className="h4-text">
                  {calculateTransport(
                    userData?.activeSubscription.isActive as boolean,
                    cartItems
                  )
                    .cartDisplaySubtotal.toLocaleString("ro-RO", {
                      style: "currency",
                      currency: "lei",
                    })
                    .toLowerCase()}
                </h4>
              </div>
            </Flex>
          </Popover.Dropdown>
        </Popover>
        <div className={classes.avatarPic}>
          <Link href="/my-account">
            <ActionIcon w="auto" variant="transparent">
              <PersonRoundedIcon className="icon" />
            </ActionIcon>
          </Link>
        </div>
      </Flex>
      <Burger
        color="var(--text-color)"
        opened={opened}
        onClick={toggle}
        hiddenFrom="sm"
        size="24px"
      />
    </div>
  );
}
