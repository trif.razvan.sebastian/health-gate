import { Flex, NumberInput } from "@mantine/core";
import useGetUserData from "../hooks/firebase/useGetUserData";
import { ICart } from "../interfaces/cartInterface";
import classes from "../modules/CartProductsList.module.css";
import updateCartQuantity from "../utils/updateCartQuantity";

export default function CartProductCard({ prod }: { prod: ICart }) {
  const { userId } = useGetUserData();
  const { id: cartId } = prod;
  return (
    <div className={classes.cartProductContainer}>
      <Flex gap={8} align="center">
        <img
          className={classes.cartProductimage}
          src={prod.productImage}
          alt="Imageinea produsului"
        />
        <div>
          <p style={{ color: "var(--dark)" }} className="p-text-truncate">
            {prod.productTitle}
          </p>

          <NumberInput
            allowDecimal={false}
            allowNegative={false}
            onChange={(quantity) =>
              updateCartQuantity({ userId, cartId, quantity })
            }
            variant="unstyled"
            size="xs"
            min={0}
            value={prod.productQuantity}
            w="3rem"
          />
        </div>
      </Flex>
      <div>
        <p className="p-text">
          {(prod.productPrice * prod.productQuantity)
            .toLocaleString("ro-RO", { style: "currency", currency: "lei" })
            .toLowerCase()}
        </p>
      </div>
    </div>
  );
}
