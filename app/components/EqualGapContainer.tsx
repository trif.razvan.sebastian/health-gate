import React from "react";

interface IEqualGapContainer {
  children: React.ReactNode;
  gap?: string;
  justify?: string;
}

export default function EqualGapContainer({
  children,
  gap,
  justify
}: IEqualGapContainer) {
  return (
    <div style={{ gap: gap ? gap : "2rem", justifyContent: justify ? justify : "normal" }} className="equal-gap">
      {children}
    </div>
  );
}
