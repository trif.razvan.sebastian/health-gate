import { Text, useMantineColorScheme } from "@mantine/core";
import classes from "../modules/LabelContainer.module.css";

interface LabelContainerPropsInterface {
  label?: string | undefined;
  children?: React.ReactNode;
  bg?: string;
  w?: string;
  borderColor?: string;
  borderWidth?: string;
  labelColor?: string;
  h?: string;
}

export default function LabelContainer(props: LabelContainerPropsInterface) {
  const { children, label, bg, w, borderColor, borderWidth, labelColor, h } =
    props;
  const { colorScheme } = useMantineColorScheme();
  const bgColor = colorScheme === "light" ? "white" : "dark-7";
  return (
    <div
      className={classes.container}
      style={{
        width: w ? w : "100%",
        height: h ? h : "auto",
      }}
    >
      <Text
        bg={bg ? bg : `var(--mantine-color-${bgColor})`}
        size="sm"
        c={labelColor ? labelColor : "var(--text-color)"}
        className={classes.floatingLabel}
      >
        {label}
      </Text>
      <div
        style={{
          border: `${borderWidth ? borderWidth : "1px"} solid ${
            borderColor ? borderColor : "grey"
          }`,
          height: h ? h : "auto",
        }}
        className={classes.wrapperBorder}
      >
        {children}
      </div>
    </div>
  );
}
