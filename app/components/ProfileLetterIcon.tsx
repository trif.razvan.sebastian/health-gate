import React from "react";
import useGetProfileLettersData from "../hooks/useGetProfileLettersData";

interface IProfileLetterIconProps {
  nameToFilterBy?: string;
  iconDimmensions: string[];
  letterDimmensions: string;
}

export default function ProfileLetterIcon({
  nameToFilterBy,
  iconDimmensions,
  letterDimmensions,
}: IProfileLetterIconProps) {
  const {
    profileLettersArr,
  }: { profileLettersArr: { color: string; letter: string }[] } =
    useGetProfileLettersData();
  //   console.log(profileLettersArr);
  const letterObject = profileLettersArr
    ?.filter(
      (item) =>
        item.letter.toLowerCase() ===
        // @ts-ignore
        nameToFilterBy?.split("").shift().toLowerCase()
    )
    .shift();
  // console.log(letterObject);
  return (
    <div
      style={{
        width: iconDimmensions[0],
        height: iconDimmensions[1],
        background: letterObject?.color ? letterObject?.color : "black",
        borderRadius: "50%",
        display: "grid",
        placeContent: "center",
        color: "white",
        fontSize: letterDimmensions,
      }}
    >
      <p>
        {letterObject?.letter
          ? letterObject?.letter
          : nameToFilterBy?.toUpperCase().split("").shift()}
      </p>
    </div>
  );
}
