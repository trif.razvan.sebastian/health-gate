import { Flex, ScrollArea } from "@mantine/core";
import ArrowForwardRoundedIcon from "@mui/icons-material/ArrowForwardRounded";
import Link from "next/link";
import useGetCart from "../../hooks/firebase/useGetCart";
import useGetMenu from "../../hooks/firebase/useGetMenu";
import useGetUserData from "../../hooks/firebase/useGetUserData";
import classes from "../../modules/Menu.module.css";
import EqualGapContainer from "../EqualGapContainer";
import SectionTitle from "../SectionTitle";
import MenuHighlightsItemCard from "./MenuHighlightsItemCard";
import { IAuth } from "../../interfaces/authInterface";

export default function MenuHighlights() {
  const { menu, displayMenu } = useGetMenu();
  const { realtimeCart } = useGetCart();
  const { userId, userData } = useGetUserData();
  return (
    <EqualGapContainer>
      <Flex mr="12rem" align="flex-end" justify="space-between">
        <SectionTitle
          text={`Alege din cele ${menu.length} de preparate @h@din meniu`}
        />
        <Link className="plain-link" href="/menu">
          <button className="tertiary-button-dark">
            Vezi meniul
            <ArrowForwardRoundedIcon className="button-icon" />
          </button>
        </Link>
      </Flex>
      <ScrollArea type="always">
        <div className={classes.menuHighlightsContainer}>
          {displayMenu.map((item, i) => (
            <MenuHighlightsItemCard
              userData={userData as IAuth}
              userId={userId}
              key={i}
              item={item}
              realtimeCart={realtimeCart}
              previewOnly
            />
          ))}
        </div>
      </ScrollArea>
    </EqualGapContainer>
  );
}
