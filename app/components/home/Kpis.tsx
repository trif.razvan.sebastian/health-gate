import classes from "../../modules/Home.module.css";

interface IKpis {
  values: { label: string | number; content: string | number }[];
  labelTop?: boolean;
}

export default function Kpis({ values, labelTop }: IKpis) {
  return (
    <div className={classes.kpisContainer}>
      {values.map((kpi, i) => (
        <div className={classes.kpiWrapper} key={i}>
          {labelTop ? (
            <>
              <p className="p-text">{kpi.content}</p>
              <p className="h4-text">{kpi.label}</p>
            </>
          ) : (
            <>
              <p className="h4-text">{kpi.label}</p>
              <p className="p-text">{kpi.content}</p>
            </>
          )}
        </div>
      ))}
    </div>
  );
}
