import Link from "next/link";
import useGetMenu from "../../hooks/firebase/useGetMenu";
import useGetUtils from "../../hooks/firebase/useGetUtils";
import classes from "../../modules/Home.module.css";
import SectionTitle from "../SectionTitle";
import Kpis from "./Kpis";

interface ISubscriptionHighlight {
  buttonHidden?: boolean;
}

export default function SubscriptionHighlight({
  buttonHidden,
}: ISubscriptionHighlight) {
  const { menu } = useGetMenu();
  const { utilsObj } = useGetUtils();

  return (
    <div
      className={`${classes.subscriptionHighlightContainer} hero-padding-container`}
    >
      <div className="equal-gap">
        <SectionTitle text="Aboneaza-te" />
        <p className="p-text">
          Daca in sfarsit esti convins ca serviciile noastre ti se potrivesc,
          indrezneste si alege unul dintre abonamentele noastre!
        </p>
        {/* <Space h="2rem" /> */}
        {buttonHidden ? null : (
          <Link href="/subscriptions">
            <button
              style={{ width: "fit-content" }}
              className="primary-button-accent"
            >
              Vezi abonamente
            </button>
          </Link>
        )}
        <Kpis
          values={[
            {
              label: utilsObj?.ordersCount as number,
              content: "Comenzi",
            },
            {
              label: menu.length as number,
              content: "Preparate",
            },
            {
              label: "1",
              content: "Orase",
            },
          ]}
        />
      </div>
      <img className={classes.subsImage} src="/subs-cook.png" alt="cook" />
    </div>
  );
}
