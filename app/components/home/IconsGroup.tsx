import classes from "../../modules/Home.module.css";
import AgricultureIcon from "@mui/icons-material/Agriculture";
import FiberNewIcon from '@mui/icons-material/FiberNew';
import LocalShippingRoundedIcon from '@mui/icons-material/LocalShippingRounded';
import { Space, Text } from "@mantine/core";
import SectionTitle from "../SectionTitle";
import EqualGapContainer from "../EqualGapContainer";

const iconsContentArr = [
  {
    icon: (
      <AgricultureIcon
        className="big-icon"
      />
    ),
    title: "De la producatori locali",
    description:
      "Folosim legume si fructe crescute cu grija si atentie fara utilizarea chimicalelor.",
  },
  {
    icon: (
      <FiberNewIcon
       className="big-icon"
      />
    ),
    title: "Ceva nou mereu",
    description:
      "Adaugam feluri noi de mancare in fiecare luna pentru a oferii o experienta diversificata",
  },
  {
    icon: (
      <LocalShippingRoundedIcon
        className="big-icon"
      />
    ),
    title: "Servicii rapide",
    description:
      "Curierii nostri fac tot posibilul ca mancarea sa ajunga la tine cat mai repede",
  },
];

export default function IconsGroup() {
  return (
    <EqualGapContainer>
    <SectionTitle text="Ce ne recomanda" />
    <div className={classes.iconsGroupFlex}>
      {iconsContentArr.map((content, i) => (
        <div key={i} className={classes.iconsIndividualGroup}>
          <div className={classes.iconWrapper}>{content.icon}</div>
          <p style={{
            textAlign: "center"
          }} className="h4-text">{content.title}</p>
          <p style={{
            textAlign: "center"
          }} className="p-text">{content.description}</p>
        </div>
      ))}
    </div>
    </EqualGapContainer>
  );
}
