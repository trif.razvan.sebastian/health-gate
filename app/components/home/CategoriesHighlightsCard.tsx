import { Flex } from "@mantine/core";
import classes from "../../modules/Menu.module.css";

interface ICategoriesHighlightsCard {
  item: { label: string; image: string; value: string };
  goToCategory: () => void;
}

export default function CategoriesHighlightsCard({
  item,
  goToCategory,
}: ICategoriesHighlightsCard) {
  return (
    <div
      onClick={goToCategory}
      style={{
        backgroundImage: `url(${item.image})`,
      }}
      className={classes.categoriesImageContainer}
    >
      <Flex
        bg="var(--dark-transparent)"
        justify="center"
        align="center"
        w="100%"
        h="100%"
        style={{
          borderRadius: 10,
        }}
      >
        <p className="h3-text-light">{item.label}</p>
      </Flex>
    </div>
  );
}
