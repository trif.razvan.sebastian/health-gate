import { Collapse, Flex } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import ArrowDownwardRoundedIcon from "@mui/icons-material/ArrowDownwardRounded";
import ArrowUpwardRoundedIcon from "@mui/icons-material/ArrowUpwardRounded";
import { useRouter, useSearchParams } from "next/navigation";
import classes from "../../modules/Menu.module.css";
import { goToCategory } from "../../utils/goToCategory";
import SectionTitle from "../SectionTitle";
import CategoriesHighlightsCard from "./CategoriesHighlightsCard";

export const categoryThumbnails = [
  {
    value: "main-course",
    label: "Felul principal",
    image:
      "https://images.unsplash.com/photo-1529566652340-2c41a1eb6d93?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHwxNHx8bWFpbiUyMGNvdXJzZXxlbnwwfHx8fDE3MDIxMzc0ODZ8MA&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
  {
    value: "dessert",
    label: "Deserturi",
    image:
      "https://images.unsplash.com/photo-1497888329096-51c27beff665?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHw3fHxkZXNlcnQlMjBmb29kfGVufDB8fHx8MTcwMjEzNzQzM3ww&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
  {
    value: "juice",
    label: "Sucuri naturale",
    image:
      "https://images.unsplash.com/photo-1583577612013-4fecf7bf8f13?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHw0fHxqdWljZXN8ZW58MHx8fHwxNzAyMTM3NTUzfDA&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
  {
    value: "snack",
    label: "Gustari",
    image:
      "https://images.unsplash.com/photo-1641423982364-306526e0f8f4?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHwyNHx8c2FuZHdpY2hlc3xlbnwwfDB8fHwxNzAyNTgwNTgyfDA&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
  {
    value: "soup",
    label: "Supe",
    image:
      "https://images.unsplash.com/photo-1607528971899-2e89e6c0ec69?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHwyfHxzb3Vwc3xlbnwwfDB8fHwxNzAyNTgwNzUyfDA&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
  {
    value: "salad",
    label: "Salate",
    image:
      "https://images.unsplash.com/photo-1537785713284-0015ce8a145c?crop=entropy&cs=srgb&fm=jpg&ixid=M3w0Mzc0NDd8MHwxfHNlYXJjaHw2fHxzYWxhZHN8ZW58MHwwfHx8MTcwMjU4MDgxNnww&ixlib=rb-4.0.3&q=85&q=85&fmt=jpg&crop=entropy&cs=tinysrgb&w=1080",
  },
];

export default function CategoriesHighlights() {
  const [opened, { toggle }] = useDisclosure(false); //collapse component for 3 of the categories
  const router = useRouter();
  const searchParams = useSearchParams();

  return (
    <div>
      <div className={classes.categoriesHighlightsGrid}>
        <div style={{ justifyContent: "center" }} className="equal-gap-26">
          <SectionTitle text="Iti punem la dispozitie 6 categorii din care poti alege" />
          <p className="p-text">
            Exploreaza arome autentice și delicii sanatoase la un singur click
            distanța si bucura-te de o experienta culinara perfect echilibrata.
          </p>
        </div>
        {categoryThumbnails.slice(0, 3).map((item, i) => {
          return (
            <CategoriesHighlightsCard
              goToCategory={() =>
                goToCategory(item.value, router, searchParams)
              }
              key={i}
              item={item}
            />
          );
        })}
      </div>
      <Collapse transitionDuration={1000} mt="2rem" in={opened}>
        <div className={classes.categoriesHighlightsGrid}>
          {categoryThumbnails.slice(3).map((item, i) => {
            return (
              <CategoriesHighlightsCard
                goToCategory={() =>
                  goToCategory(item.value, router, searchParams)
                }
                key={i}
                item={item}
              />
            );
          })}
        </div>
      </Collapse>
      <Flex mt="1rem" justify="center">
        <button onClick={toggle} className="tertiary-button-dark">
          Vezi mai multe{" "}
          {opened ? (
            <ArrowUpwardRoundedIcon className="button-icon" />
          ) : (
            <ArrowDownwardRoundedIcon className="button-icon" />
          )}
        </button>
      </Flex>
    </div>
  );
}
