import { Flex } from "@mantine/core";
import classes from "../../modules/Home.module.css";
import ArrowForwardRoundedIcon from "@mui/icons-material/ArrowForwardRounded";
import Link from "next/link";

export default function Hero() {
  return (
    <div className={`${classes.heroContainer} hero-padding-container`}>
      <div style={{ gap: "2.5rem" }} className="equal-gap">
        <h1 className="h1-text">
          Nu sacrifica gustul pentru sanatate - noi le oferim pe amandoua
        </h1>
        <p className="p-text">
          Exploreaza arome autentice și delicii sanatoase la un singur click
          distanța si bucura-te de o experienta culinara perfect echilibrata.
        </p>
        <Flex align="center" gap="2.5rem">
          <Link href="/menu">
            <button className="primary-button-dark">Vezi meniul</button>
          </Link>
          <Link className="plain-link" href="/how-it-works">
            <button className="tertiary-button-dark">
              Afla mai multe
              <ArrowForwardRoundedIcon className="button-icon" />
            </button>
          </Link>
        </Flex>
      </div>
      <img src="/hero-cook.png" className={classes.heroImage} alt="cook" />
    </div>
  );
}
