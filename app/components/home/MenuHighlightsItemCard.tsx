import { ActionIcon, Flex, HoverCard, Tooltip } from "@mantine/core";
import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded";
import ModeCommentRoundedIcon from "@mui/icons-material/ModeCommentRounded";
import MoreVertRoundedIcon from "@mui/icons-material/MoreVertRounded";
import { useRouter, useSearchParams } from "next/navigation";
import { IAuth } from "../../interfaces/authInterface";
import { ICart } from "../../interfaces/cartInterface";
import { IMenu } from "../../interfaces/menuInterface";
import classes from "../../modules/Menu.module.css";
import addToCart from "../../utils/addToCart";
import openProduct from "../../utils/openProduct";
import removeFromFavorites from "../../utils/removeFromFavorites";

interface IMenuHighlightsItemCardProps {
  item: IMenu;
  realtimeCart: ICart[];
  openProductModal?: (id: string) => void;
  previewOnly?: boolean;
  favoritesSection?: boolean;
  userId: string;
  userData: IAuth;
}

export default function MenuHighlightsItemCard({
  item,
  realtimeCart,
  openProductModal,
  previewOnly,
  favoritesSection,
  userData,
  userId,
}: IMenuHighlightsItemCardProps) {
  const { price, id: productId, image, title } = item;
  const router = useRouter();
  const searchParams = useSearchParams();
  const buttonCondition =
    realtimeCart.length &&
    realtimeCart?.map((prod) => prod.productId).includes(productId as string);
  const lastMenuOrder = userData?.lastMenuOrder as string;
  const isActive = userData?.activeSubscription.isActive as boolean;

  return (
    <div
      onClick={() => {
        if (previewOnly) return;
        openProductModal && openProductModal(item.id as string);
      }}
      className={classes.menuProductCard}
    >
      <div style={{ position: "relative" }}>
        {favoritesSection && (
          <div className={classes.floatingFavoriteButtonContainer}>
            <HoverCard width={150} position="bottom" shadow="md">
              <HoverCard.Target>
                <ActionIcon c="var(--footer-color)" variant="transparent">
                  <MoreVertRoundedIcon />
                </ActionIcon>
              </HoverCard.Target>
              <HoverCard.Dropdown p={0}>
                <button
                  onClick={() =>
                    removeFromFavorites(
                      userId,
                      productId as string,
                      userData?.favorites as string[]
                    )
                  }
                  style={{
                    fontSize: ".8rem",
                  }}
                  className="unstyled-button"
                >
                  <Flex gap={2} align="center">
                    <DeleteRoundedIcon
                      style={{ color: "var(--text-color)" }}
                      className="small-icon"
                    />
                    <p className="small-text">Elimina</p>
                  </Flex>
                </button>
                <button
                  onClick={() =>
                    openProduct(productId as string, router, searchParams)
                  }
                  style={{
                    fontSize: ".8rem",
                  }}
                  className="unstyled-button"
                >
                  <Flex gap={3} align="center">
                    <ModeCommentRoundedIcon
                      style={{ color: "var(--text-color)" }}
                      className="small-icon"
                    />
                    <p className="small-text">Mergi la produs</p>
                  </Flex>
                </button>
              </HoverCard.Dropdown>
            </HoverCard>
          </div>
        )}
        <img
          className={classes.menuProductImage}
          src={item.image}
          alt={item.title}
        />
        <Flex justify="space-between" align="center">
          <Tooltip label={item.title}>
            <p className="p-text-truncate">{item.title}</p>
          </Tooltip>
          <p
            className="p-text"
            style={{ fontWeight: 600, fontSize: "1.25rem" }}
          >
            {item.price}lei
          </p>
        </Flex>
        <button
          onClick={(event) =>
            addToCart({
              price,
              productId,
              userId,
              image,
              title,
              realtimeCart,
              userData,
              isActive,
              lastMenuOrder,
              event,
            })
          }
          className={
            buttonCondition
              ? "primary-button-dark-full"
              : "secondary-button-dark-full"
          }
        >
          {buttonCondition ? "In cos | +1" : "Adauga in cos"}
        </button>
      </div>
    </div>
  );
}
