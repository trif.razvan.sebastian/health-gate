"use client";
import { useState } from "react";
import { TextInput, Button, NumberInput, Flex } from "@mantine/core";
import useAddSubscriptionTemplate from "../templates/adminConsole/useAddSubscriptionTemplate";
import addSubscription from "../utils/adminConsole/addSubscription";
export default function SubscriptionsForm() {
  const { addSubscriptionTemplate } = useAddSubscriptionTemplate();
  const [valueToAdd, setValueToAdd] = useState("");

  const addBenefit = () => {
    if (valueToAdd) {
      addSubscriptionTemplate.insertListItem("benefits", valueToAdd);
      setValueToAdd("");
    }
  };

  return (
    <form
      onSubmit={addSubscriptionTemplate.onSubmit((values) =>
        addSubscription({ values, addSubscriptionTemplate })
      )}
    >
      <TextInput
        {...addSubscriptionTemplate.getInputProps("title")}
        size="xs"
        label="Title"
      />
      <TextInput
        {...addSubscriptionTemplate.getInputProps("description")}
        size="xs"
        label="Description"
      />
      <TextInput
        {...addSubscriptionTemplate.getInputProps("code")}
        size="xs"
        label="Code"
      />
      <TextInput
        {...addSubscriptionTemplate.getInputProps("plan")}
        size="xs"
        label="Plan"
      />
      <NumberInput
        {...addSubscriptionTemplate.getInputProps("price")}
        size="xs"
        label="Price"
      />
      <Flex gap="1rem" align="flex-end">
        <TextInput
          value={valueToAdd}
          onChange={(e) => setValueToAdd(e.target.value)}
          size="xs"
          label="Benefits"
        />
        <Button
          variant="transparent"
          size="compact-sm"
          onClick={addBenefit}
          type="button"
        >
          Add to list
        </Button>
      </Flex>
      <ul>
        {addSubscriptionTemplate.values.benefits?.map((item, i) => (
          <li key={i}>{item}</li>
        ))}
      </ul>
      <Button type="submit">Submit</Button>
    </form>
  );
}
