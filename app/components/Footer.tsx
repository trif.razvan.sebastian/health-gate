import LocalPhoneRoundedIcon from "@mui/icons-material/LocalPhoneRounded";
import EmailRoundedIcon from "@mui/icons-material/EmailRounded";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import Link from "next/link";
import classes from "../modules/Home.module.css";

export default function Footer() {
  return (
    <div className={classes.footer}>
      <div className={classes.footerGroups}>
        <h4 className="h4-text">Despre Health Gate</h4>
        <p className="p-text">Informatii</p>
        <p className="p-text">Planul nostru</p>
        <p className="p-text">Termeni si conditii</p>
      </div>
      <div className={classes.footerGroups}>
        <h4 className="h4-text">Contact</h4>
        <Link
          href="/"
          style={{ textDecoration: "none" }}
          className={classes.footerContactLinks}
        >
          <LocalPhoneRoundedIcon className="footer-icon" />
          <p className="p-text">+40123456789</p>
        </Link>
        <Link
          href="/"
          style={{ textDecoration: "none" }}
          className={classes.footerContactLinks}
        >
          <EmailRoundedIcon className="footer-icon" />
          <p className="p-text">healthgate@gmail.com</p>
        </Link>
        <Link
          href="/"
          style={{ textDecoration: "none" }}
          className={classes.footerContactLinks}
        >
          <FacebookRoundedIcon className="footer-icon" />
          <p className="p-text">HealthGate Official</p>
        </Link>
      </div>
      <div className={classes.footerGroups}>
        <h2 style={{textAlign: "end"}} className="h2-text">V 1.0.0</h2>
        <p className="p-text">
          made by Trif Razvan with <span style={{ color: "red" }}>❤</span>
        </p>
      </div>
    </div>
  );
}
