"use client";

import {
  Select,
  useComputedColorScheme,
  useMantineColorScheme,
  useMantineTheme,
} from "@mantine/core";
import WbSunnyIcon from "@mui/icons-material/WbSunny";
import Brightness3Icon from "@mui/icons-material/Brightness3";

export default function ThemeChangeSwitch() {
  const { colorScheme, setColorScheme } = useMantineColorScheme();
  const computedColorScheme = useComputedColorScheme("light");
  const theme = useMantineTheme();

  const changeTheme = () => {
    setColorScheme(computedColorScheme === "light" ? "dark" : "light");
  };

  return (
    // it breaks after refresh but works
    <Select
      leftSection={
        colorScheme === "light" ? (
          <WbSunnyIcon
            style={{
              // color: "var(--text-color)",
              color: theme.colors.yellow[5],
              width: "1.1rem",
            }}
          />
        ) : (
          <Brightness3Icon
            style={{
              // color: "var(--text-color)",
              color: theme.colors.blue[2],
              width: "1.1rem",
            }}
          />
        )
      }
      clearable={false}
      value={colorScheme}
      size="xs"
      w="5.5rem"
      data={[
        { value: "light", label: "Light" },
        { value: "dark", label: "Dark" },
      ]}
      onChange={() => changeTheme()}
    />
  );

  // return computedColorScheme === "dark" ? (
  //   <ActionIcon
  //   w="auto"
  //     style={{ borderRadius: 0, borderBottom: "2px solid var(--text-color)" }}
  //     color="var(--text-color)"
  //     onClick={() => setColorScheme("light")}
  //     variant="subtle"
  //   >
  //     {sunIcon}
  //   </ActionIcon>
  // ) : (
  //   <ActionIcon
  //   w="auto"
  //     style={{ borderRadius: 0, borderBottom: "2px solid var(--text-color)" }}
  //     color="var(--text-color)"
  //     onClick={() => setColorScheme("dark")}
  //     variant="subtle"
  //   >
  //   {moonIcon}
  //   </ActionIcon>
  // );
}
