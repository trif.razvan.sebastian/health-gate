import Link from "next/link";
import useGetCart from "../hooks/firebase/useGetCart";


export default function CartCheckoutButton() {
    const {realtimeCart} = useGetCart()
    const disabledCondition = realtimeCart.length ? false : true
  return (
    <Link href="/checkout">
    <button disabled={disabledCondition} className={`primary-button-${disabledCondition ? "disabled" : "accent"}`}>Comanda</button>
    </Link>

  )
}
