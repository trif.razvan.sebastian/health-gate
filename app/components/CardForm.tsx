import { TextInput, Flex } from "@mantine/core";
import { UseFormReturnType } from "@mantine/form";
import { ICheckout, IOrderCheckout } from "../interfaces/checkoutInterface";

interface ICardFormProps {
  checkoutTemplate: UseFormReturnType<ICheckout | IOrderCheckout>;
}

export default function CardForm({ checkoutTemplate }: ICardFormProps) {
  return (
    <div>
      <TextInput
        {...checkoutTemplate.getInputProps("cardData.cardOwner")}
        required
        w="100%"
        size="xs"
        label="Titular card"
      />
      <TextInput
        {...checkoutTemplate.getInputProps("cardData.cardNumber")}
        required
        w="100%"
        size="xs"
        label="Numar card"
      />
      <div>
        <Flex gap=".8rem">
          <TextInput
            {...checkoutTemplate.getInputProps(
              "cardData.cardExpDate.cardExpMonth"
            )}
            required
            w="6rem"
            size="xs"
            label="Luna expirare"
          />
          <TextInput
            {...checkoutTemplate.getInputProps(
              "cardData.cardExpDate.cardExpYear"
            )}
            required
            w="6rem"
            size="xs"
            label="Anul expirare"
          />
        </Flex>
      </div>
      <TextInput
        {...checkoutTemplate.getInputProps("cardData.cardCvv")}
        required
        w="100%"
        size="xs"
        label="Cvv"
      />
    </div>
  );
}
