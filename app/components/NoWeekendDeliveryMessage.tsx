import { Flex } from "@mantine/core";

interface INoWeekendDeliveryMessage {
  mt?: string;
}

export default function NoWeekendDeliveryMessage({
  mt,
}: INoWeekendDeliveryMessage) {
  return (
    <Flex mt={mt} p={5} bg="var(--accent-red-op)" justify="center">
      <p className="p-text" style={{ color: "var(--accent-red)" }}>
        Nu livram in weekend. Ne vedem luni!
      </p>
    </Flex>
  );
}
