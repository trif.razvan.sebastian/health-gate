import { Carousel } from "@mantine/carousel";
import Autoplay from "embla-carousel-autoplay";
import { Flex } from "@mantine/core";
import { useRef } from "react";
import classes from "../modules/Home.module.css";

const dummyArr1 = [
  "https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_1280.jpg",
  "https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_1280.jpg",
  "https://cdn.pixabay.com/photo/2015/12/01/20/28/road-1072823_1280.jpg",
];
const dummyArr = [
  "banner_1.png",
  "banner_1.png",
  "banner_1.png",

];

export default function ImagesCarrousel() {
  const autoplay = useRef(Autoplay());
  return (
    <Flex justify="center">
      <Carousel
        // @ts-ignore
        plugins={[autoplay.current]}
        onMouseEnter={autoplay.current.stop}
        onMouseLeave={autoplay.current.reset}
        w="100%"
        loop
        h={300}
        // bg="blue"
        // withIndicators
      >
        {dummyArr.map((img, i) => (
          <Carousel.Slide key={i}>
            <Flex justify="center">
              <div
                style={{
                  backgroundImage: `url(${img})`,
                  height: "300px",
                  width: "100%",
                  // aspectRatio: "16/9",
                  backgroundSize: "contain",
                  backgroundRepeat: "no-repeat",
                  padding: "1rem 3rem",
                  backgroundColor: "red"
                }}
              >
                <h1>hello</h1>
                <div className={classes.accessButtonContainer}>
                  <button>helo</button>
                </div>
              </div>
            </Flex>
          </Carousel.Slide>
        ))}
      </Carousel>
    </Flex>
  );
}
