import { TextInput } from "@mantine/core";
import { UseFormReturnType } from "@mantine/form";
import { ICheckout, IOrderCheckout } from "../interfaces/checkoutInterface";

interface IAdressFormProps {
  flexAdressLines?: boolean;
  checkoutTemplate: UseFormReturnType<ICheckout | IOrderCheckout>;
}

export default function AdressForm({
  flexAdressLines,
  checkoutTemplate,
}: IAdressFormProps) {
  return (
    <div>
      <div
        style={{
          display: flexAdressLines ? "flex" : "block",
          gap: ".8rem",
        }}
      >
        <TextInput
          {...checkoutTemplate.getInputProps("adressData.primaryAdress")}
          required
          w="100%"
          size="xs"
          label="Adresa 1"
        />
        <TextInput
          {...checkoutTemplate.getInputProps("adressData.secondaryAdress")}
          w="100%"
          size="xs"
          label="Adresa 2"
        />
      </div>
      <TextInput
        {...checkoutTemplate.getInputProps("adressData.postalCode")}
        required
        w="100%"
        size="xs"
        label="Cod postal"
      />
    </div>
  );
}
