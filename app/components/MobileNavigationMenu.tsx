import Link from "next/link";
import classes from "../modules/MobileNavigationMenu.module.css";
import {
  ActionIcon,
  Avatar,
  Button,
  Divider,
  Flex,
  Space,
  Text,
} from "@mantine/core";
import LogoutIcon from "@mui/icons-material/Logout";
import MapOutlinedIcon from "@mui/icons-material/MapOutlined";
import ThemeChangeSwitch from "./ThemeChangeSwitch";

interface IMobileNavigationMenuProps {
  navigationRoutes: { label: string; route: string; icon: React.ReactNode }[];
}

export default function MobileNavigationMenu({
  navigationRoutes,
}: IMobileNavigationMenuProps) {
  return (
    <div>
      <Space h="1rem" />
      <Divider
        mb="1.2rem"
        color="var(--primary-color)"
        label="Cont"
        labelPosition="left"
      />
      <Flex justify="space-between" align="center">
        <Button p={0} variant="subtle" component={Link} href="/my-account">
          <Avatar variant="transparent" color="var(--text-color)" size="md" />
          <Text c="var(--text-color)">Buna, Trif Razvan!</Text>
        </Button>
        <ActionIcon variant="subtle">
          <LogoutIcon style={{ color: "var(--text-color)" }} />
        </ActionIcon>
      </Flex>
      <Divider
        mt="2rem"
        mb="1.2rem"
        color="var(--primary-color)"
        label="Navigare"
        labelPosition="left"
      />
      {navigationRoutes.map((nav, i) => (
        <div key={i} className={classes.linkFlex}>
          <Link href={`${nav.route}`}>{nav.label}</Link>
          {nav.icon}
        </div>
      ))}
      <Divider
        mt="2rem"
        mb="1.2rem"
        color="var(--primary-color)"
        label="Actiuni"
        labelPosition="left"
      />
      <Flex justify="space-between" align="flex-start">
        <div className={classes.linkFlex}>
          <Text>Navigare Rapida</Text>
          <MapOutlinedIcon />
        </div>
        <ThemeChangeSwitch />
        {/* <div className={classes.linkFlex}>
          <Text>Setari</Text>
          <SettingsIcon />
        </div> */}
      </Flex>
    </div>
  );
}
