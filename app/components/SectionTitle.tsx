
interface IsectionTitleProps {
  text: string;
  wr?: string;
}

export default function SectionTitle({ text, wr }: IsectionTitleProps) {
  const newText = text.split("@h@")
  return (
    <p className="h2-text">
      {newText[0]}
      {newText.length > 1 && <br />}{newText[1]}
    </p>
  );
}
