"use client";

import {
  ActionIcon,
  AppShell,
  Avatar,
  Burger,
  Button,
  Flex,
  Popover,
  Space,
  Text,
  TextInput,
} from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import classes from "../modules/NavigationLayout.module.css";
import Link from "next/link";
import StorefrontIcon from "@mui/icons-material/Storefront";
import ManageSearchIcon from "@mui/icons-material/ManageSearch";
import PaymentIcon from "@mui/icons-material/Payment";
import RestaurantMenuIcon from "@mui/icons-material/RestaurantMenu";
import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import MobileNavigationMenu from "./MobileNavigationMenu";
import ThemeChangeSwitch from "./ThemeChangeSwitch";
import { usePathname } from "next/navigation";
import SectionTitle from "./SectionTitle";
import Footer from "./Footer";
import Navbar from "./Navbar";

export default function NavigationLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const pathname = usePathname();
  const [opened, { toggle }] = useDisclosure();

  const navigationRoutes = [
    { label: "Abonamente", route: "/subscriptions", icon: <PaymentIcon /> },
    { label: "Meniu", route: "/menu", icon: <RestaurantMenuIcon /> },
    { label: "Cum comand", route: "/how-it-works", icon: <HelpOutlineIcon /> },
  ];

  const authPaths = ["/login", "/signup", "/terms-and-conditions"];

  return (
    <AppShell
      disabled={authPaths.includes(pathname)}
      header={{ height: "127px" }}
      navbar={{
        width: 300,
        breakpoint: "sm",
        collapsed: { mobile: !opened, desktop: !opened },
      }}
      padding={0}
      // py="1.5rem"
    >
      <AppShell.Header
        h="fit-content"
        // zIndex={0}
        withBorder={false}
        bg="var(--background-color)"
        className={`${classes.headerContainer} padding-container`}
      >
        <Navbar />
      </AppShell.Header>

      <AppShell.Navbar p="sm">
        <MobileNavigationMenu navigationRoutes={navigationRoutes} />
      </AppShell.Navbar>

      <AppShell.Main pb={pathname === "/subscriptions" ? "22rem" : "6rem"} bg="var(--background-color)" >{children}</AppShell.Main>
      <Footer />
    </AppShell>
  );
}
