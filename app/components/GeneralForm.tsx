import {
  TextInput,
  Select,
  NumberInput,
  Flex,
  ActionIcon,
} from "@mantine/core";
import useGetUserData from "../hooks/firebase/useGetUserData";
import { UseFormReturnType } from "@mantine/form";
import { ICheckout, IOrderCheckout } from "../interfaces/checkoutInterface";
import PersonIcon from "@mui/icons-material/Person";

interface IGeneralFormProps {
  checkoutTemplate: UseFormReturnType<ICheckout | IOrderCheckout>;
}

export default function GeneralForm({ checkoutTemplate }: IGeneralFormProps) {
  // maybe get userData as props from the parent
  const { userData } = useGetUserData();

  const autocompleteField = (fieldName: string) => {
    // @ts-ignore
    checkoutTemplate.setFieldValue(fieldName, userData?.[fieldName]);
  };

  return (
    <div>
      <Flex align="flex-end" gap="md">
        <TextInput
          {...checkoutTemplate.getInputProps("fullName")}
          required
          w="100%"
          size="xs"
          label="Nume intreg"
        />
        <ActionIcon
          onClick={() => autocompleteField("fullName")}
          color="green"
          radius="20%"
          variant="light"
          mb=".5px"
        >
          <PersonIcon style={{ fontSize: 16 }} />
        </ActionIcon>
      </Flex>
      <Flex align="flex-end" gap="md">
        <TextInput
          {...checkoutTemplate.getInputProps("email")}
          required
          w="100%"
          size="xs"
          label="Email"
        />
        <ActionIcon
          onClick={() => autocompleteField("email")}
          color="green"
          radius="20%"
          variant="light"
          mb=".5px"
        >
          <PersonIcon style={{ fontSize: 16 }} />
        </ActionIcon>
      </Flex>
      {userData?.phoneNumbers.length! ? (
        <Select
          {...checkoutTemplate.getInputProps("phoneNumber")}
          required
          placeholder="Alege un numar de telefon"
          size="xs"
          label="Numar de telefon principal"
          data={userData?.phoneNumbers}
        />
      ) : (
        <NumberInput
          {...checkoutTemplate.getInputProps("phoneNumber")}
          required={!userData?.phoneNumbers.length!}
          w="100%"
          size="xs"
          label="Numar de telefon"
          hideControls
        />
      )}
    </div>
  );
}
