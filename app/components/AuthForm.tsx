"use client";

import {
  Button,
  PasswordInput,
  TextInput,
  Space,
  Title,
  Checkbox,
  Text,
} from "@mantine/core";
import classes from "../modules/AuthForm.module.css";
import useAuthFormTemplate from "../templates/useAuthFormTemplate";
import Link from "next/link";
import { IAuth, ILogin, ISignUp } from "../interfaces/authInterface";
import { useRouter } from "next/navigation";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";

// USE AN IF STATEMENT WITH A STRING TO DICTATE WHICH FUNCTION SHOULD BE USED

interface IAuthFormProps {
  title: string;
  onSubmitFunc: (values: any, router: AppRouterInstance) => void;
  isSignup?: boolean;
  buttonTitle?: string;
  alternativeRoute: React.ReactNode;
}

export default function AuthForm({
  title,
  onSubmitFunc,
  isSignup,
  buttonTitle,
  alternativeRoute,
}: IAuthFormProps) {
  const { authFormTemplate } = useAuthFormTemplate();
  const router = useRouter();
  return (
    <form
      onSubmit={authFormTemplate.onSubmit((values) =>
        onSubmitFunc(values, router)
      )}
      className={classes.formContainer}
    >
      <Title c="var(--text-color)">{title}</Title>
      <Space h="sm" />
      {isSignup ? (
        <TextInput
          size="xs"
          {...authFormTemplate.getInputProps("fullName")}
          label="Nume intreg"
          required
        />
      ) : null}
      <TextInput
        size="xs"
        {...authFormTemplate.getInputProps("email")}
        label="Email"
        required
      />
      <PasswordInput
        size="xs"
        {...authFormTemplate.getInputProps("password")}
        label="Parola"
        required
      />
      {isSignup ? (
        <PasswordInput
          size="xs"
          {...authFormTemplate.getInputProps("confirmPassword")}
          label="Confirma parola"
          required
        />
      ) : null}
      <Space h="sm" />
      <div className={classes.actionPart}>
        {isSignup ? (
          <Checkbox
            color="var(--primary-color)"
            {...authFormTemplate.getInputProps("termsAndConditionsChecked", {
              type: "checkbox",
            })}
            label={
              <Text c="black" size="sm">
                Accept{" "}
                <Link
                  style={{ color: "var(--primary-color)" }}
                  href="/terms-and-conditions"
                >
                  termenii si conditiile.
                </Link>
              </Text>
            }
          />
        ) : null}
        {!isSignup && <div></div>}
        <Button
          type="submit"
          className={classes.submitButton}
          color="var(--primary-color)"
        >
          {buttonTitle ? buttonTitle : title}
        </Button>
      </div>
      <Space h="sm" />
      {alternativeRoute}
    </form>
  );
}
