import { ICart } from "../interfaces/cartInterface";
import classes from "../modules/CartProductsList.module.css";
import CartProductCard from "./CartProductCard";

export default function CartProductsList({
  realtimeCart,
}: {
  realtimeCart: ICart[];
}) {
  return realtimeCart?.length ? (
    <div className={classes.cartContainer}>
      {realtimeCart?.map((prod, i) => (
        <CartProductCard key={i} prod={prod} />
      ))}
    </div>
  ) : (
    <div>
      <p className="p-text">Nu aveti produse in cos..</p>
    </div>
  );
}
