import { useForm } from "@mantine/form";
import { IAddMenuOfTheDay } from "../../interfaces/menuOfTheDayInterface";

export default function useAddMenuOfTheDay() {
  const addMenuOfTheDayTemplate = useForm<IAddMenuOfTheDay>({
    initialValues: {
      menuItemOne: "",
      menuItemTwo: "",
      menuTotal: 0,
      forDay: ""
    },
  });

  return { addMenuOfTheDayTemplate };
}
