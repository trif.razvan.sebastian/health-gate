import { useForm } from "@mantine/form";
import { IAddSubscription } from "../../interfaces/addSubscriptionInterface";

export default function useAddSubscriptionTemplate() {
  const addSubscriptionTemplate = useForm<IAddSubscription>({
    initialValues: {
      title: "",
      price: 0,
      benefits: [],
      code: "",
      plan: "", //basic, pro+, pro++,
      description: "",
      rules: {
        label: "",
        interval: "",
        intervalCount: 0,
      },
      discountId: ""
    },
  });

  return { addSubscriptionTemplate };
}
