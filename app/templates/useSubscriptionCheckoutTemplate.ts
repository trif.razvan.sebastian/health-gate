import { useForm } from "@mantine/form";
import { ICheckout } from "../interfaces/checkoutInterface";
import moment from "moment";

export default function useSubscriptionCheckoutTemplate() {
  const subscriptionCheckoutTemplate = useForm<ICheckout>({
    initialValues: {
      fullName: "",
      email: "",
      phoneNumber: "",
      adressData: {
        primaryAdress: "",
        secondaryAdress: "",
        postalCode: "",
      },
      paymentType: "",
      cardData: {
        cardOwner: "",
        cardNumber: "",
        cardExpDate: {
          cardExpMonth: "",
          cardExpYear: "",
        },
        cardCvv: "",
      },
    },
    validate: {
      cardData: {
        cardOwner: (value) =>
          value.length > 2 ? null : "Introdu numele titularului",

        cardNumber: (value) =>
          /^\d+$/.test(value) && value.length === 16
            ? null
            : "Introdu numarul cardului",
        cardExpDate: {
          cardExpMonth: (value) =>
            /^0*\d/g.test(value) &&
            [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].includes(parseInt(value))
              ? null
              : "Completeaza data",
          cardExpYear: (value) =>
            +value >= +moment().format("YY") && value.length === 2
              ? null
              : "Compleateaza data",
        },
        cardCvv: (value) =>
          /\d+/.test(value) && value.length === 3
            ? null
            : "Introdu un cvv valid",
      },
    },
  });

  return { subscriptionCheckoutTemplate };
}
