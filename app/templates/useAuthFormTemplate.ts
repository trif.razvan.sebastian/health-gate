import { useForm } from "@mantine/form";
import { IAuth } from "../interfaces/authInterface";
import { usePathname } from "next/navigation";
import { useEffect } from "react";

const useAuthFormTemplate = (cloudData?: IAuth) => {
  const pathname = usePathname();
  const authFormTemplate = useForm<IAuth>({
    initialValues: {
      fullName: "",
      email: "",
      phoneNumbers: [],
      password: "",
      confirmPassword: "",
      termsAndConditionsChecked: false,
      createdAt: "",
      updatedAt: "",
      orders: 0,
      ordersStreak: 0,
      activeSubscription: {
        subscriptionId: "",
        subscriptionDiscountAmount: 0,
        isActive: false,
        period: {
          start: "",
          end: "",
          update: "",
        },
        stripeSubscriptionId: "",
        cancelAtPeriodEnd: false,
        discountId: ""
      },
      favorites: [],
      lastMenuOrder: "",
    },
    validate: {
      password: (value, values) =>
        pathname !== "/signup"
          ? null
          : value === values.confirmPassword
          ? null
          : "Parola nu coincide cu confirmarea!",
      confirmPassword: (value, values) =>
        pathname !== "/signup"
          ? null
          : value === values.password
          ? null
          : "Confirmarea nu coincide cu parola!",
      termsAndConditionsChecked: (value) =>
        pathname !== "/signup"
          ? null
          : value === false
          ? "Bifeaza casuta pentru a continua!"
          : null,
      fullName: (value) =>
        pathname !== "/signup"
          ? null
          : value.length >= 2
          ? null
          : "Numele trebuie sa contina cel putin 2 litere!",
      email: (value) =>
        /^\S+@\S+$/.test(value) ? null : "Introdu un email valid!",
      phoneNumbers: (value) =>
        value.length > 2 ? "Nu poti sa adaugi decat 2 numere de telefon" : null,
    },
    validateInputOnChange: false,
  });

  useEffect(() => {
    if (cloudData) {
      authFormTemplate.setValues(cloudData);
    }
  }, [cloudData]);

  return { authFormTemplate };
};

export default useAuthFormTemplate;
