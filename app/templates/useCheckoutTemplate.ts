import { useForm } from "@mantine/form";
import {
  INewCheckout,
  OrderStatus,
  PaymentMethod,
  PaymentStatus,
} from "../interfaces/checkoutInterface";

export default function useCheckoutTemplate() {
  const checkoutFormTemplate = useForm<INewCheckout>({
    initialValues: {
      orderId: 0,
      customer: {
        customerId: "",
        fullName: "",
        email: "",
        phone: "",
        address: {
          line1: "",
          line2: "",
          city: "",
          county: "",
          postalCode: "",
        },
      },
      items: [],
      subtotal: 0,
      tax: 0,
      total: 0,
      shipping: 0,
      discount: 0,
      payment: {
        method: PaymentMethod.CASH,
        paymentStatus: PaymentStatus.PROCESSING_PAYMENT,
      },
      status: OrderStatus.PROCESSING,
      createdAt: "",
      updatedAt: "",
      sessionId: "",
    },
  });

  return { checkoutFormTemplate };
}

// items: sessionLineItems?.data.map((prod, i) => ({
//     productId: jsonArr[i].split("/")[0] || "undef",
//     productQuantity: prod.quantity || "undef",
//     productCurrency: prod.currency || "undef",
//     productPrice: prod.amount_total / 100 || "undef",
//     cartId: jsonArr[i].split("/")[1] || "undef",
//     //   productTax: prod.amount_tax,
//   })),

// productId: "",
// productQuantity: 1,
// productCurrency: "ron",
// productPrice: 0,
// cartId: "",
