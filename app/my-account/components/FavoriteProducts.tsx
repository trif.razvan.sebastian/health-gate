import { ScrollArea, Space } from "@mantine/core";
import SectionTitle from "../../components/SectionTitle";
import MenuHighlightsItemCard from "../../components/home/MenuHighlightsItemCard";
import useGetCart from "../../hooks/firebase/useGetCart";
import { IAuth } from "../../interfaces/authInterface";
import { IMenu } from "../../interfaces/menuInterface";
import classes from "../../modules/Menu.module.css";

interface IUserData {
  menu: IMenu[];
  userData: IAuth;
}

export default function FavoriteProducts({ menu, userData }: IUserData) {
  const { realtimeCart } = useGetCart();
  const favoriteProducts = menu.filter((item) =>
    userData?.favorites.includes(item.id as string)
  );

  return (
    <div>
      <div style={{ gap: "2.5rem" }} className="equal-gap">
        <SectionTitle text="Produse favorite" />
        <p style={{ width: "30%" }} className="p-text">
          Adauga produsele favorite in lista pentru a le gasi mai usor data
          viitoare cand vrei sa le comanzi.
        </p>
      </div>
      <Space h={26} />
      <ScrollArea type="always">
        <div className={classes.menuHighlightsContainer}>
          {favoriteProducts.map((item, i) => (
            <MenuHighlightsItemCard
              userData={userData as IAuth}
              userId={userData.id as string}
              realtimeCart={realtimeCart}
              previewOnly
              item={item}
              key={i}
              favoritesSection
            />
          ))}
        </div>
      </ScrollArea>
    </div>
  );
}
