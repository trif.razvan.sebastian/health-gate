import { TextInput } from "@mantine/core";
import { auth } from "../../firebase/firebase";
import useCheckObjectDifferences from "../../hooks/useCheckObjectDifferences";
import { IAuth } from "../../interfaces/authInterface";
import useAuthFormTemplate from "../../templates/useAuthFormTemplate";
import updateUser from "../../utils/updateUser";
import MultiplePhoneNumbers from "./MultiplePhoneNumbers";

export const inputStyle = {
  label: {
    color: "var(--background-color)",
  },
  description: {
    color: "var(--light-transparent)",
  },
  input: {
    backgroundColor: "var(--footer-color)",
  },
};

export default function AccountForm({
  userData,
}: {
  userData: IAuth | undefined;
}) {
  const { authFormTemplate } = useAuthFormTemplate(userData);
  const { password, confirmPassword, termsAndConditionsChecked, ...rest } =
    authFormTemplate.values;
  const { isSubmitDisabled } = useCheckObjectDifferences({
    rest,
    userData,
    authFormTemplate,
  });

  return (
    <form
      onSubmit={authFormTemplate.onSubmit((values) =>
        updateUser(userData?.id, values, userData, auth.currentUser)
      )}
    >
      <TextInput
        styles={inputStyle}
        width="100%"
        label="Numele intreg"
        {...authFormTemplate.getInputProps("fullName")}
        size="xs"
      />
      <MultiplePhoneNumbers
        inputStyle={inputStyle}
        userData={userData}
        authFormTemplate={authFormTemplate}
      />
      <button
        className={
          isSubmitDisabled ? "primary-button-disabled" : "primary-button-accent"
        }
        disabled={isSubmitDisabled}
        type="submit"
      >
        Actualizeaza date
      </button>
    </form>
  );
}
