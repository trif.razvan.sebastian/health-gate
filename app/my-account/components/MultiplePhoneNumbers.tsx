"use client";
import { ActionIcon, Divider, TextInput } from "@mantine/core";
import { UseFormReturnType } from "@mantine/form";
import AddIcon from "@mui/icons-material/Add";
import DeleteOutlineOutlinedIcon from "@mui/icons-material/DeleteOutlineOutlined";
import { ChangeEvent, useCallback, useState } from "react";
import { IAuth } from "../../interfaces/authInterface";
import deletePhoneNumber from "../../utils/deletePhoneNumber";

const phoneNumberRegex = /^\d{10}$/;

interface IMultiplePhoneNumbers {
  justDisplay?: boolean;
  authFormTemplate: UseFormReturnType<IAuth>;
  userData?: IAuth;
  inputStyle?: {};
}

export default function MultiplePhoneNumbers({
  justDisplay,
  authFormTemplate,
  userData,
  inputStyle,
}: IMultiplePhoneNumbers) {
  const phoneNumbersRef = authFormTemplate?.getInputProps("phoneNumbers").value;
  const [valueToAdd, setValueToAdd] = useState("");
  const buttonConditions =
    phoneNumberRegex.test(valueToAdd) &&
    authFormTemplate.values.phoneNumbers.length < 2;

  const addPhoneNumbers = useCallback(() => {
    if (
      phoneNumberRegex.test(valueToAdd) &&
      authFormTemplate.values.phoneNumbers.length < 2
    ) {
      authFormTemplate?.insertListItem("phoneNumbers", valueToAdd);
      setValueToAdd("");
    }
  }, [valueToAdd]);

  const removeValue = (value: string) => {
    const index = phoneNumbersRef.indexOf(value);
    authFormTemplate?.removeListItem("phoneNumbers", index);
  };

  return (
    <div>
      {justDisplay ? null : (
        <div
          style={{
            width: "100%",
            display: "flex",
            alignItems: "flex-end",
            gap: "var(--mantine-spacing-md)",
          }}
        >
          <TextInput
            styles={inputStyle}
            description="Poti adauga maxim 2 numere de telefon!"
            value={valueToAdd}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setValueToAdd(e.target.value)
            }
            w="100%"
            label="Adauga numere de telefon"
            size="xs"
            type="number"
          />
          <ActionIcon
            onClick={addPhoneNumbers}
            type="button"
            color={
              buttonConditions ? "var(--primary-color)" : "var(--footer-color)"
            }
            size={30}
            variant={"filled"}
          >
            <AddIcon
              style={{
                fontSize: 16,
                color: buttonConditions ? "white" : "var(--dark-transparent)",
              }}
            />
          </ActionIcon>
        </div>
      )}
      {phoneNumbersRef?.length ? (
        <div>
          <Divider my="sm" />
          {phoneNumbersRef?.map((item: string, i: number) => {
            return (
              <TextInput
                styles={inputStyle}
                readOnly
                key={i}
                label={`Numar de telefon ${i + 1}`}
                value={item}
                size="xs"
                rightSection={
                  <ActionIcon
                    disabled={
                      userData?.phoneNumbers.length !==
                      authFormTemplate.values.phoneNumbers.length
                    }
                    onClick={() =>
                      deletePhoneNumber(
                        userData?.id,
                        i,
                        authFormTemplate.values.phoneNumbers
                      )
                    }
                    c={
                      userData?.phoneNumbers.length !==
                      authFormTemplate.values.phoneNumbers.length
                        ? "var(--text-color)"
                        : "var(--primary-color)"
                    }
                    variant="transparent"
                  >
                    <DeleteOutlineOutlinedIcon style={{ fontSize: 16 }} />
                  </ActionIcon>
                }
              />
            );
          })}
          <Divider my="md" />
        </div>
      ) : null}
    </div>
  );
}
