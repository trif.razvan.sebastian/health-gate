import { Flex, TextInput } from "@mantine/core";
import EqualGapContainer from "../../components/EqualGapContainer";
import { inputStyle } from "./GeneralInfo";

interface IAddressInfo {
  line1: string;
  line2: string;
  postalCode: string;
}

export default function AddressInfo({
  line1,
  line2,
  postalCode,
}: IAddressInfo) {
  return (
    <div>
      <Flex mb="1rem" justify="center">
        <p className="p-text-dark">Informatii adresa:</p>
      </Flex>
      <EqualGapContainer gap="1rem">
        <TextInput
          styles={inputStyle}
          label="Adresa linie 1"
          readOnly
          defaultValue={line1}
          w="100%"
        />
        <TextInput
          styles={inputStyle}
          label="Adresa linie 2"
          readOnly
          defaultValue={line2}
          w="100%"
        />
        <TextInput
          styles={inputStyle}
          label="Cod postal"
          readOnly
          defaultValue={postalCode}
          w="100%"
        />
      </EqualGapContainer>
    </div>
  );
}
