import { Badge, Flex, ScrollArea } from "@mantine/core";

export const tagsStyle = [
  {
    value: "CASH",
    color: "#4caf50",
    bgColor: "#66d177", // 20% lighter shade of "#4caf50"
  },
  {
    value: "CARD",
    color: "#4C9DAF",
    bgColor: "#73b8c1", // 20% lighter shade of "#4C9DAF"
  },
  {
    value: "MENIU_GRATUIT",
    color: "#AFA54C",
    bgColor: "#c8be66", // 20% lighter shade of "#AFA54C"
  },
  {
    value: "TRANSPORT_GRATUIT",
    color: "#544CAF",
    bgColor: "#6d5fb8", // 20% lighter shade of "#544CAF"
  },
];

export default function TagsInfo({ tags }: { tags: string[] }) {
  const combined = tags.map((tag) => {
    const prop = tagsStyle.find((item) => item.value === tag.replace(" ", "_"));
    return {
      label: tag,
      color: prop?.color,
      bgColor: prop?.bgColor,
    };
  });
  return (
    <ScrollArea type="auto">
      <Flex gap="1rem" align="center">
        <p className="p-text-dark">Taguri:</p>
        {combined.map((tag, i) => (
          <Badge
            key={i}
            size="lg"
            variant="light"
            color={tag.bgColor}
            c={tag.color}
          >
            {tag.label}
          </Badge>
        ))}
      </Flex>
    </ScrollArea>
  );
}
