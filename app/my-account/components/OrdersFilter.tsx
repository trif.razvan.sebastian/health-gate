import { ActionIcon, Badge, Flex, ScrollArea } from "@mantine/core";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { useRouter, useSearchParams } from "next/navigation";
import EqualGapContainer from "../../components/EqualGapContainer";
import { tagsStyle } from "./TagsInfo";
import applyOrderFilter from "../../utils/applyOrderFilter";

export default function OrdersFilter() {
  const router = useRouter();
  const searchParams = useSearchParams();
  const params = new URLSearchParams(searchParams);

  const deleteFilters = () => {
    if (params.size <= 0) return;
    router.replace("/my-account", { scroll: false });
  };

  const filtersList = tagsStyle.map((tag, i) => {
    const isValue = params.get(tag.value.toLowerCase());
    const color = isValue ? tag.color : "#07120899";
    const bgColor = isValue ? tag.color : "#0712083a";

    return (
      <ActionIcon
        onClick={() => applyOrderFilter(tag.value, router, searchParams)}
        w="auto"
        variant="transparent"
        key={i}
      >
        <Badge size="lg" variant="light" color={bgColor} c={color}>
          {tag.value.replace("_", " ")}
        </Badge>
      </ActionIcon>
    );
  });

  return (
    <div>
      <EqualGapContainer gap="1rem">
        <Flex
          justify="space-between"
          align="center"
          style={{
            borderBottom: "1px solid var(--dark)",
          }}
        >
          <p className="p-text-dark">Filtreaza:</p>
          <ActionIcon onClick={deleteFilters} variant="transparent">
            <CloseRoundedIcon className="button-icon" />
          </ActionIcon>
        </Flex>
        <ScrollArea offsetScrollbars type="hover">
          <Flex gap="1rem">{filtersList}</Flex>
        </ScrollArea>
      </EqualGapContainer>
    </div>
  );
}
