import { Space } from "@mantine/core";
import SectionTitle from "../../components/SectionTitle";

export default function DiscountsSection() {
  return (
    <div>
      {" "}
      <div style={{ gap: "2.5rem" }} className="equal-gap">
        <SectionTitle text="Reduceri castigate" />
        <p style={{width: "40%"}} className="p-text">
          Toate discount-urile castigate intr-un singur loc asteptandu-te sa le
          folosesti.
        </p>
      </div>
      <Space h={26} />
    </div>
  );
}
