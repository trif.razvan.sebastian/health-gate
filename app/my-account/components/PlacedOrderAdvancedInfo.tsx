import EqualGapContainer from "../../components/EqualGapContainer";
import { INewCheckout } from "../../interfaces/checkoutInterface";
import classes from "../../modules/MyAccount.module.css";
import AddressInfo from "./AddressInfo";
import GeneralInfo from "./GeneralInfo";
import TagsInfo from "./TagsInfo";

interface IPlacedOrderAdvancedInfo {
  order: INewCheckout;
}

export default function PlacedOrderAdvancedInfo({
  order,
}: IPlacedOrderAdvancedInfo) {
  return (
    <div className={classes.advancedInfoContainer}>
      <EqualGapContainer gap="26px">
        <TagsInfo tags={order.tags as string[]} />
        <GeneralInfo
          name={order.customer.fullName}
          phone={order.customer.phone}
          email={order.customer.email}
        />
        <AddressInfo
          line1={order.customer.address.line1}
          line2={order.customer.address.line2}
          postalCode={order.customer.address.postalCode}
        />
      </EqualGapContainer>
    </div>
  );
}
