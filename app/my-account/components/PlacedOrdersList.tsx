import { Flex, Space } from "@mantine/core";
import { useSearchParams } from "next/navigation";
import EqualGapContainer from "../../components/EqualGapContainer";
import SectionTitle from "../../components/SectionTitle";
import useGetUtils from "../../hooks/firebase/useGetUtils";
import { INewCheckout } from "../../interfaces/checkoutInterface";
import { IMenu } from "../../interfaces/menuInterface";
import { IUtilsObj } from "../../interfaces/utilsObjInterface";
import OrdersFilter from "./OrdersFilter";
import PlacedOrderItem from "./PlacedOrderItem";
import { tagsStyle } from "./TagsInfo";

interface IUserData {
  orders: INewCheckout[];
  menu: IMenu[];
}

export default function PlacedOrdersList({ menu, orders }: IUserData) {
  const searchParams = useSearchParams();
  const params = new URLSearchParams(searchParams);
  const { utilsObj } = useGetUtils();
  const paramsTagsArr = tagsStyle
    .filter((tag) => params.get(tag.value.toLowerCase()))
    .map((item) => item.value);

  const returnProduct = (productId: string) => {
    let tempProduct;
    tempProduct = menu.find((item) => item.id === productId);
    return tempProduct as IMenu;
  };

  const filteredOrders = orders.filter((order) => {
    const includesAllTags = paramsTagsArr.every((item) =>
      order.tags?.includes(item.replace("_", " "))
    );
    return includesAllTags;
  });

  return (
    <div>
      <Flex justify="space-between">
        <div style={{ gap: "2.5rem" }} className="equal-gap">
          <SectionTitle text="Comenzi plasate" />
          <p style={{ width: "80%" }} className="p-text">
            Aici poti vedea toate comenzile pe care le-ai plasat si detalii
            despre acestea.
          </p>
        </div>
        <OrdersFilter />
      </Flex>
      <Space h={26} />
      <EqualGapContainer gap="26px">
        {filteredOrders?.map((order, i) => (
          <PlacedOrderItem
            utilsObj={utilsObj as IUtilsObj}
            returnProduct={returnProduct}
            key={i}
            order={order}
          />
        ))}
      </EqualGapContainer>
    </div>
  );
}
