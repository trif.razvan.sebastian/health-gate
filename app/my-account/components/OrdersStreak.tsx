import { Button, Text, ActionIcon } from "@mantine/core";
import React, { useState } from "react";
import RedeemIcon from '@mui/icons-material/Redeem';

interface IOrdersStreakProps {
  orders?: [];
  ordersStreak?: number;
}

export default function OrdersStreak({
  orders,
  ordersStreak,
}: IOrdersStreakProps) {
  return (
    <div style={{ display: "flex", gap: "2px", alignItems: "center" }}>
      <Text mr="10px" size="sm" c="var(--text-color)">
        Streak de comenzi:
      </Text>

      {Array(25)
        .fill(null)
        .map((_, i) => (
          <div
            key={i}
            style={{
              backgroundColor:
                i < ordersStreak! ? "var(--primary-color)" : "#cccccc",
              borderRadius: 5,
              border: i < ordersStreak! ? "none" : "1px solid #b0aeae",
              width: "2rem",
              height: "5px",
            }}
          ></div>
        ))}
      {ordersStreak === 25 ? (
        <ActionIcon ml={10} variant="subtle" c="var(--primary-color)"><RedeemIcon style={{fontSize: "1.5rem"}}/></ActionIcon>
      ) : (
        <Text
          ml="10px"
          fw={ordersStreak === 25 ? 700 : 400}
          size="sm"
          c={ordersStreak === 25 ? "var(--primary-color)" : "var(--text-color)"}
        >
          {ordersStreak}/25
        </Text>
      )}
    </div>
  );
}
