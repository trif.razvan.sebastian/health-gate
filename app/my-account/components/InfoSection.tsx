import {
  ActionIcon,
  Card,
  Center,
  Flex,
  RingProgress,
  Text,
  Tooltip,
} from "@mantine/core";
import CheckRoundedIcon from "@mui/icons-material/CheckRounded";
import LogoutIcon from "@mui/icons-material/Logout";
import { useRouter } from "next/navigation";
import EqualGapContainer from "../../components/EqualGapContainer";
import ProfileLetterIcon from "../../components/ProfileLetterIcon";
import Kpis from "../../components/home/Kpis";
import useGetSubscriptionPlan from "../../hooks/firebase/useGetSubscriptionPlan";
import useGetSubscriptions from "../../hooks/firebase/useGetSubscriptions";
import { IAuth } from "../../interfaces/authInterface";
import { INewCheckout } from "../../interfaces/checkoutInterface";
import classes from "../../modules/MyAccount.module.css";
import logoutUser from "../../utils/logoutUser";
import AccountForm from "./AccountForm";

interface IUserData {
  userData: IAuth;
  userId: string;
  orders: INewCheckout[];
}

export default function InfoSection({ userData, userId, orders }: IUserData) {
  const { subscriptions } = useGetSubscriptions();
  const { subscriptionPlan } = useGetSubscriptionPlan({
    subscriptions,
    userData,
  });
  const router = useRouter();

  const percentOrdersStreak = () => {
    const streak = userData?.ordersStreak as number;
    return (streak / 25) * 100;
  };

  return (
    <div className={classes.backgroundCard}>
      <div className={classes.flexContainer}>
        <Card
          bg="var(--footer-color)"
          w="24rem"
          shadow="sm"
          padding={20}
          radius={10}
          withBorder
        >
          <EqualGapContainer>
            <Flex justify="center">
              <Tooltip
                events={{ hover: true, focus: true, touch: true }}
                label={`${userData?.ordersStreak}/25`}
                c="white"
                color="#4caf4fa2"
              >
                <RingProgress
                  size={182}
                  sections={[
                    {
                      value: percentOrdersStreak(),
                      color: "var(--primary-color)",
                    },
                  ]}
                  label={
                    percentOrdersStreak() === 100 ? (
                      <Center>
                        <ActionIcon
                          color="teal"
                          variant="light"
                          radius="50%"
                          size="6rem"
                        >
                          <CheckRoundedIcon style={{ fontSize: "3rem" }} />
                        </ActionIcon>
                      </Center>
                    ) : (
                      <Center>
                        <ProfileLetterIcon
                          nameToFilterBy={userData?.fullName}
                          iconDimmensions={["8rem", "8rem"]}
                          letterDimmensions="4rem"
                        />
                      </Center>
                    )
                  }
                />
              </Tooltip>
            </Flex>
            <div className={classes.flexGreating}>
              <h2 className="h2-text" style={{ color: "var(--primary-color)" }}>
                Buna
                <span
                  className="h3-text-dark"
                  style={{ fontWeight: 400, color: "var(--text-color)" }}
                >
                  , {userData?.fullName}!
                </span>
              </h2>
              <ActionIcon variant="transparent">
                <LogoutIcon
                  onClick={() => logoutUser(router)}
                  style={{ color: "var(--text-color)" }}
                />
              </ActionIcon>
            </div>
            <Flex justify="center">
              <Kpis
                values={[
                  { content: "Comenzi", label: orders.length },
                  { content: "Abonament", label: subscriptionPlan },
                  {
                    content: "Favorite",
                    label: userData?.favorites.length as number,
                  },
                ]}
              />
            </Flex>
            <div className={classes.createdAtContainer}>
              <Text mt="md" ta="center" size="xs" c="var(--text-color)">
                Inpreuna din {userData?.createdAt?.split(",").shift()}.
              </Text>
            </div>
          </EqualGapContainer>
        </Card>
        <div style={{ width: "50%" }}>
          <AccountForm userData={userData} />
        </div>
      </div>
    </div>
  );
}
