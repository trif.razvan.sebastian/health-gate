import { Flex } from "@mantine/core";
import Image from "next/image";
import EqualGapContainer from "../../components/EqualGapContainer";
import { ICheckoutLineItems } from "../../interfaces/cartInterface";
import { IMenu } from "../../interfaces/menuInterface";
import { IUtilsObj } from "../../interfaces/utilsObjInterface";
import classes from "../../modules/MyAccount.module.css";

interface IOrderProductCard {
  item: ICheckoutLineItems;
  returnProduct: (productId: string) => IMenu;
  utilsObj: IUtilsObj;
}

export default function OrderProductCard({
  item,
  returnProduct,
  utilsObj,
}: IOrderProductCard) {
  const product = returnProduct(item.productId);

  const returnImage = () => {
    if (item.menuItems?.length && utilsObj && utilsObj.menuImage) {
      return utilsObj.menuImage as string;
    }
    return product?.image as string;
  };

  return (
    <div>
      <Flex align="center" gap={10}>
        <img
        style={{
          width: 180,
          aspectRatio: 1/1
        }}
          className={classes.orderProductImage}
          src={returnImage && returnImage()}
          alt={product?.title ? product.title : "Meniul zilei"}
        />
        <Flex w="100%" align="center" justify="space-between">
          <EqualGapContainer gap="8px">
            <p className="p-text-dark">
              {product?.title ? product.title : "Meniul zilei"}
            </p>
            <p className={` ${classes.breakDescription} p-text`}>
              {product?.description
                ? product.description
                : "Combinatie dintre 2 preparate atent alese pentru clientii nostri."}
            </p>
          </EqualGapContainer>
          <div
            style={{ alignItems: "flex-end" }}
            className={classes.basicInfoTextContainer}
          >
            <p style={{ fontSize: "1.5rem" }} className="p-text">
              {item.productPrice
                .toLocaleString("ro-RO", {
                  style: "currency",
                  currency: "lei",
                })
                .toLowerCase()}
            </p>
            <p className="p-text">x{item.productQuantity}</p>
          </div>
        </Flex>
      </Flex>
    </div>
  );
}
