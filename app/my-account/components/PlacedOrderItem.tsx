import { ActionIcon, Collapse, Flex } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import AirportShuttleRoundedIcon from "@mui/icons-material/AirportShuttleRounded";
import CheckRoundedIcon from "@mui/icons-material/CheckRounded";
import ClearRoundedIcon from "@mui/icons-material/ClearRounded";
import ExpandLessRoundedIcon from "@mui/icons-material/ExpandLessRounded";
import ExpandMoreRoundedIcon from "@mui/icons-material/ExpandMoreRounded";
import RoomServiceIcon from "@mui/icons-material/RoomService";
import EqualGapContainer from "../../components/EqualGapContainer";
import { INewCheckout } from "../../interfaces/checkoutInterface";
import { IMenu } from "../../interfaces/menuInterface";
import { IUtilsObj } from "../../interfaces/utilsObjInterface";
import classes from "../../modules/MyAccount.module.css";
import OrderProductCard from "./OrderProductCard";
import PlacedOrderAdvancedInfo from "./PlacedOrderAdvancedInfo";

interface IPlacedOrderItem {
  order: INewCheckout;
  returnProduct: (productId: string) => IMenu;
  utilsObj: IUtilsObj;
}

const statusValues = [
  {
    value: "COOKING",
    color: "var(--accent-red-vibrant)",
    label: "Se pregateste",
    icon: <RoomServiceIcon style={{ fontSize: 14 }} />,
  },
  {
    value: "SHIPPING",
    color: "var(--accent-yellow)",
    label: "In drum spre tine",
    icon: <AirportShuttleRoundedIcon style={{ fontSize: 14 }} />,
  },
  {
    value: "DELIVERED",
    color: "var(--primary-color)",
    label: "Livrata la",
    icon: <CheckRoundedIcon style={{ fontSize: 14 }} />,
  },
  {
    value: "CANCELED",
    color: "var(--dark)",
    label: "Anulata",
    icon: <ClearRoundedIcon style={{ fontSize: 14 }} />,
  },
];

export default function PlacedOrderItem({
  order,
  returnProduct,
  utilsObj,
}: IPlacedOrderItem) {
  const [opened, { toggle }] = useDisclosure(false);

  const displayOrderNumber = `#HG0${order.orderId}`;
  const returnStatus = statusValues.find((item) => item.value === order.status);

  return (
    <div onClick={toggle}>
      <div className={classes.basicInfoContainer}>
        <Flex justify="space-between">
          <Flex gap={45}>
            <div className={classes.basicInfoTextContainer}>
              <p className="p-text-dark">Numar comanda</p>
              <p className="p-text">{displayOrderNumber}</p>
            </div>
            <div className={classes.basicInfoTextContainer}>
              <p className="p-text-dark">Data plasare comanda</p>
              <p className="p-text">{order.createdAt}</p>
            </div>
            <div className={classes.basicInfoTextContainer}>
              <p className="p-text-dark">Total comanda</p>
              <p className="p-text">
                {order.total
                  ?.toLocaleString("ro-RO", {
                    style: "currency",
                    currency: "lei",
                  })
                  .toLowerCase()}
              </p>
            </div>
          </Flex>
          <div
            style={{ alignItems: "flex-end" }}
            className={classes.basicInfoTextContainer}
          >
            <Flex gap={6} align="center">
              <div
                style={{
                  backgroundColor: returnStatus?.color,
                  color: "white",
                }}
                className={classes.iconWrapper}
              >
                {returnStatus?.icon}
              </div>
              <p className="p-text">{returnStatus?.label}</p>
            </Flex>
            <ActionIcon onClick={toggle} c="var(--dark)" variant="transparent">
              {opened ? (
                <ExpandLessRoundedIcon
                  style={{
                    fontSize: 24,
                  }}
                />
              ) : (
                <ExpandMoreRoundedIcon
                  style={{
                    fontSize: 24,
                  }}
                />
              )}
            </ActionIcon>
          </div>
        </Flex>
      </div>
      <Collapse transitionDuration={1000} mt="2rem" in={opened}>
        <Flex gap="26px" justify="space-between">
          <div style={{ width: "100%" }}>
            <EqualGapContainer gap="26px">
              {order.items.map((item, i) => (
                <OrderProductCard
                  utilsObj={utilsObj}
                  key={i}
                  returnProduct={returnProduct}
                  item={item}
                />
              ))}
            </EqualGapContainer>
          </div>
          <div style={{ width: "50%" }}>
            <PlacedOrderAdvancedInfo order={order} />
          </div>
        </Flex>
      </Collapse>
    </div>
  );
}
