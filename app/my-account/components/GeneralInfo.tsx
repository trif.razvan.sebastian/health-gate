import { Flex, TextInput } from "@mantine/core";

interface IGeneralInfo {
  name: string;
  phone: string;
  email: string;
}

export const inputStyle = {
  input: {
    background: "none",
    color: "var(--text-color)",
    // borderRadius: "10px",
  },
};

export default function GeneralInfo({ name, phone, email }: IGeneralInfo) {
  return (
    <div>
      <Flex mb="1rem" justify="center">
        <p className="p-text-dark">Informatii generale:</p>
      </Flex>

      <Flex mb="1rem" gap="1rem" justify="space-between">
        <TextInput
          styles={inputStyle}
          label="Nume client"
          readOnly
          defaultValue={name}
          w="100%"
        />
        <TextInput
          styles={inputStyle}
          label="Telefon client"
          readOnly
          defaultValue={phone}
          w="100%"
        />
      </Flex>
      <TextInput
        styles={inputStyle}
        label="Email client"
        readOnly
        defaultValue={email}
        w="100%"
      />
    </div>
  );
}
