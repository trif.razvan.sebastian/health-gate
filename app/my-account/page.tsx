"use client";

import { Loader } from "@mantine/core";
import { onAuthStateChanged } from "firebase/auth";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import EqualGapContainer from "../components/EqualGapContainer";
import { auth } from "../firebase/firebase";
import useGetMenu from "../hooks/firebase/useGetMenu";
import useGetOrders from "../hooks/firebase/useGetOrders";
import useGetUserData from "../hooks/firebase/useGetUserData";
import DiscountsSection from "./components/DiscountsSection";
import FavoriteProducts from "./components/FavoriteProducts";
import InfoSection from "./components/InfoSection";
import PlacedOrdersList from "./components/PlacedOrdersList";

export default function MyAccount() {
  const router = useRouter();
  const { userData, userId } = useGetUserData();
  const { orders } = useGetOrders(userData?.id as string);
  const { menu } = useGetMenu();

  useEffect(() => {
    const check = onAuthStateChanged(auth, (user) => {
      if (user) {
        console.log("user logged in");
        return;
      } else {
        router.push("/login");
      }
    });
    return () => check();
  }, []);

  return userData?.id ? (
    <EqualGapContainer gap="6rem">
      <div className="margin-container">
        <InfoSection userData={userData} userId={userId} orders={orders} />
      </div>
      {orders.length ? (
        <div className="margin-container">
          <PlacedOrdersList menu={menu} orders={orders} />
        </div>
      ) : null}
      {userData.favorites.length ? (
        <div className="margin-container-left">
          <FavoriteProducts menu={menu} userData={userData} />
        </div>
      ) : null}
      {false && (
        <div className="margin-container">
          <DiscountsSection />
        </div>
      )}
    </EqualGapContainer>
  ) : (
    <Loader color="var(--primary-color)" />
  );
}
