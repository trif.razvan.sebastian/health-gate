"use client";
import { Loader } from "@mantine/core";
import SubscriptionHighlight from "../components/home/SubscriptionHighlight";
import useGetSubscriptions from "../hooks/firebase/useGetSubscriptions";
import useGetUserData from "../hooks/firebase/useGetUserData";
import { IAuth } from "../interfaces/authInterface";
import classes from "../modules/Subscriptions.module.css";
import SubscriptionTemplate from "./components/SubscriptionTemplate";

export default function Subscriptions() {
  const { subscriptions } = useGetSubscriptions();
  const { userData, userId } = useGetUserData();
  return subscriptions?.length ? (
    <div>
      <div className={classes.subscriptionsRelativeContainer}>
        <SubscriptionHighlight buttonHidden />
      </div>
      <div className={classes.subscriptionsFloatingContainer}>
        <div className={classes.subscriptionsFlexContainer}>
          {subscriptions?.map((sub, i) => (
            <SubscriptionTemplate
              userData={userData as IAuth}
              userId={userId}
              activeSubscription={userData?.activeSubscription}
              key={i}
              // @ts-ignore
              sub={sub}
              i={i}
            />
          ))}
        </div>
      </div>
    </div>
  ) : (
    <Loader size="lg" color="var(--primary-color)" />
  );
}
