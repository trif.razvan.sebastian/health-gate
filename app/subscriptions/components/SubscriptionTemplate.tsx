import { Badge, Flex, List } from "@mantine/core";
import CheckRoundedIcon from "@mui/icons-material/CheckRounded";
import EqualGapContainer from "../../components/EqualGapContainer";
import {
  ISubscription,
  IUserSubscription,
} from "../../interfaces/addSubscriptionInterface";
import { IAuth } from "../../interfaces/authInterface";
import classes from "../../modules/Subscriptions.module.css";
import subscribe from "../../utils/subscribe";
import unsubscribe from "../../utils/unsubscribe";

interface ISubscriptionTemplateProps {
  sub: ISubscription;
  i?: number;
  disableButton?: boolean;
  activeSubscription?: IUserSubscription | null;
  userData: IAuth;
  userId: string;
}

export default function SubscriptionTemplate({
  sub,
  i,
  disableButton,
  activeSubscription,
  userData,
  userId,
}: ISubscriptionTemplateProps) {
  // const { colorScheme } = useMantineColorScheme();
  const {
    title,
    description,
    benefits,
    code,
    plan,
    price,
    rules: { interval, intervalCount, label },
    id,
    discountId,
  } = sub;

  return (
    <div className={classes.templateWrapper}>
      <EqualGapContainer justify="space-between">
        <Flex align="center" justify="space-between">
          <p className="p-text-accent">{sub.plan}</p>
          {activeSubscription?.subscriptionId === sub.id && (
            <Badge color="var(--light-blue)" c="var(--primary-color)">
              Abonat
            </Badge>
          )}
        </Flex>
        <div className={classes.priceWrapper}>
          <Flex align="baseline">
            <h1 className="text-60">{sub.price}</h1>
            <h2 className="h2-text">lei</h2>
          </Flex>
          <Flex w="100%" justify="flex-end">
            <p className="p-text-dark">/{sub.rules.label}</p>
          </Flex>
        </div>
        <div>
          <List
            c="var(--dark)"
            icon={
              <CheckRoundedIcon
                style={{ color: "var(--primary-color)", fontSize: "1rem" }}
              />
            }
          >
            {benefits?.sort().map((benefit, i) => (
              <List.Item lh="160%" key={i}>
                {benefit}
              </List.Item>
            ))}
          </List>
        </div>
        {activeSubscription?.subscriptionId === sub.id ? (
          <button
            className="secondary-button-dark-full"
            onClick={() =>
              unsubscribe(
                userData?.activeSubscription.stripeSubscriptionId as string
              )
            }
          >
            Dezabonare
          </button>
        ) : (
          <button
            disabled={activeSubscription?.isActive}
            className={
              activeSubscription?.isActive
                ? "primary-button-disabled"
                : "primary-button-accent-full"
            }
            onClick={() =>
              subscribe({
                title,
                price,
                id,
                interval,
                intervalCount,
                userId,
                discountId,
              })
            }
          >
            Abonare
          </button>
        )}
      </EqualGapContainer>
    </div>
  );
}
