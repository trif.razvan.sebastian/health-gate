import { NextRequest, NextResponse } from "next/server";
import Stripe from "stripe";
import checkSubscriptionOrder from "../../utils/checkSubscriptionOrder";
import calculateTransport from "../../utils/calculateTransport";

export async function POST(req: NextRequest) {
  const stripe = new Stripe(process.env.NEXT_PUBLIC_STRIPE_KEY!);
  const data = await req.json();
  const {
    isActive,
    cartItems,
    lastMenuOrder,
  } = data;
  try {
    const hasFreeMenu = checkSubscriptionOrder({
      isActive,
      lastMenuOrder,
      cartItems,
    })?.hasFreeMenu;
    const session = await stripe.checkout.sessions.create({
      // @ts-ignore
      line_items: cartItems?.map((item) => ({
        price_data: {
          currency: "ron",
          product_data: {
            name: item.title,
            images: [item.image],
            metadata: { product_id: "item.id", cartId: "item.cartId" },
          },
          unit_amount: item.price,
        },
        quantity: item.quantity,
      })),
      metadata: {
        user_id: data.userId,
        product_ids: JSON.stringify(
          // @ts-ignore
          data.cartItems?.map((item) => `${item.id}/${item.cartId}`)
        ),
        address_id: `${data.addressId}`,
        orders_count: data.ordersCount,
        full_name: data.fullName,
        phone: data.phoneNumber,
        menu_items: JSON.stringify(
          // @ts-ignore
          data.cartItems?.map((item) => item.menuItems)
        ),
        has_free_menu: JSON.stringify(hasFreeMenu),
      },
      automatic_tax: {
        enabled: true,
      },
      shipping_options: [
        {
          shipping_rate_data: {
            type: "fixed_amount",
            fixed_amount: {
              amount: calculateTransport(isActive, cartItems)
                .formatedTransportTotal,
              currency: "ron",
            },
            display_name: "Transport",
          },
        },
      ],
      mode: "payment",
      allow_promotion_codes: true,
      success_url: `http://localhost:3000`,
      cancel_url: `http://localhost:3000/checkout`,
      locale: "ro",
    });

    return NextResponse.json(session.url);
  } catch (error) {
    console.log(error);
    // @ts-ignore
    console.log(error.message);
    return NextResponse.json(
      // @ts-ignore
      { error: error.message },
      { status: 500 }
    );
  }
}
