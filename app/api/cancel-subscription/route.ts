import { NextRequest, NextResponse } from "next/server";
import Stripe from "stripe";
import { ESubscriptionActions } from "../../interfaces/addSubscriptionInterface";

export async function GET(req: NextRequest) {
  const stripe = new Stripe(process.env.NEXT_PUBLIC_STRIPE_KEY!);

  try {
    const stripeSubscriptionId = req.nextUrl.searchParams.get(
      "stripeSubscriptionId"
    );
    if (!stripeSubscriptionId) {
      return NextResponse.json(
        { error: "Missing stripeSubscriptionId in query parameters" },
        { status: 400 }
      );
    }

    const session = await stripe.subscriptions.update(stripeSubscriptionId, {
      cancel_at_period_end: true,
      metadata: {
        action: ESubscriptionActions.UPDATE,
      },
    });
    return NextResponse.json({
      session: session,
      status: 200,
    });
  } catch (error) {
    console.log(error);
    // @ts-ignore
    console.log(error.message);
    return NextResponse.json(
      // @ts-ignore
      { error: error.message },
      { status: 500 }
    );
  }
}
