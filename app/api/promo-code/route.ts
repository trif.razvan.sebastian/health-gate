import { NextRequest, NextResponse } from "next/server";
import Stripe from "stripe";

export async function POST(req: NextRequest) {
  const stripe = new Stripe(process.env.NEXT_PUBLIC_STRIPE_KEY!);
  const data = await req.json();

  try {
    const promoCode = await stripe.promotionCodes.create({
      coupon: data.couponId,
      expires_at: data.dateEnd,
    });

    return NextResponse.json({ promoCode }, { status: 200 });
  } catch (error) {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
      },
      { status: 500 }
    );
  }
}
