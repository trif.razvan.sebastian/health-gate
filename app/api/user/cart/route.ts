import { collection, getDocs } from "firebase/firestore";
import { NextRequest, NextResponse } from "next/server";
import { db } from "../../../firebase/firebase";
import { ICart } from "../../../interfaces/cartInterface";

export async function GET(req: NextRequest) {
  const id = req.nextUrl.searchParams.get("id");
  const cart: ICart[] = [];
  if (!id) {
    return NextResponse.json({ message: "Can't get the id" }, { status: 500 });
  }
  try {
    const querySnapshot = await getDocs(
      collection(db, "users", id as string, "cart")
    );
    querySnapshot.forEach((doc) => {
      // @ts-ignore
      cart.push({ ...doc.data(), id: doc.id });
    });

    return NextResponse.json(cart || []);
  } catch (error) {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
      },
      {
        status: 500,
      }
    );
  }
}
