import {
  collection,
  doc,
  onSnapshot,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import moment from "moment";
import { NextRequest, NextResponse } from "next/server";
import { db } from "../../../firebase/firebase";
import { IAuth } from "../../../interfaces/authInterface";

export async function PATCH(
  req: NextRequest,
  { params }: { params: { id: string } }
) {
  const data = await req.json();
  const { values, userData, user } = data;

  try {
    const docRef = doc(db, "users", params.id);
    const momentDate = moment().format("DD.MM.YYYY, hh:mm:ss");

    // basic data-----------------------------------------------------

    if (values.fullName !== userData?.fullName) {
      await updateDoc(docRef, {
        fullName: values.fullName,
        updatedAt: momentDate,
      });
      return NextResponse.json(
        {
          message: "Updated basic data!",
        },
        { status: 200 }
      );
    }

    // phone numbers--------------------------------------------------

    if (values.phoneNumbers.length !== userData?.phoneNumbers.length) {
      await updateDoc(docRef, {
        phoneNumbers: values.phoneNumbers,
        updatedAt: momentDate,
      });
      return NextResponse.json(
        {
          message: "Updated phone numbers!",
        },
        { status: 200 }
      );
    }
  } catch (error) {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
        message: "Unable to update user!",
      },
      { status: 500 }
    );
  }
}
