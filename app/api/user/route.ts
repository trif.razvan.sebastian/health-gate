import { createUserWithEmailAndPassword } from "firebase/auth";
import { addDoc, collection, getDocs, query, where } from "firebase/firestore";
import moment from "moment";
import { NextRequest, NextResponse } from "next/server";
import { auth, db } from "../../firebase/firebase";

export async function POST(req: NextRequest) {
  if (req.method !== "POST") {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
        message: "Not allowed!",
      },
      { status: 405 }
    );
  }

  const data = await req.json();

  const {
    fullName,
    email,
    password,
    ordersStreak,
    orders,
    activeSubscription,
    phoneNumbers,
    favorites,
  } = data.values;

  try {
    const momentDate = moment().format("DD.MM.YYYY, hh:mm:ss");
    let userData;
    createUserWithEmailAndPassword(auth, email, password as string).then(
      async () => {
        try {
          const docRef = await addDoc(collection(db, "users"), {
            fullName,
            email,
            phoneNumbers,
            activeSubscription,
            createdAt: momentDate,
            updatedAt: momentDate,
            orders,
            ordersStreak,
            favorites,
            lastMenuOrder: "",
          });
          userData = docRef.firestore.toJSON();
          return NextResponse.json(
            {
              userData: docRef.firestore.toJSON(),
              message: "User created successfully!",
            },
            { status: 201 }
          );
        } catch (error) {
          return NextResponse.json(
            {
              // @ts-ignore
              error: error.message,
            },
            { status: 500 }
          );
        }
      }
    );
    return NextResponse.json(
      {
        userData,
        message: "User created successfully!",
      },
      { status: 201 }
    );
  } catch (error) {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
        message: "Unable to create user!",
      },
      { status: 500 }
    );
  }
}

export async function GET(req: NextRequest) {
  const email = req.nextUrl.searchParams.get("email");

  if (!email) {
    return NextResponse.json(
      {
        message: "No email",
      },
      { status: 500 }
    );
  }

  try {
    let userData;
    let userId;
    const queryUser = query(
      collection(db, "users"),
      where("email", "==", email)
    );

    const querySnapshot = await getDocs(queryUser);
    querySnapshot.forEach((user) => {
      userData = { ...user.data(), id: user.id };
      userId = user.id;
    });

    return NextResponse.json(
      {
        userData,
        userId: `${userId}`,
      },
      {
        status: 200,
      }
    );
  } catch (error) {
    return NextResponse.json(
      {
        // @ts-ignore
        error: error.message,
        message: "Unable to get user data!",
      },
      { status: 500 }
    );
  }

  //   try {
  //     let userData;
  //     let userId;
  //     const queryUser = query(
  //       collection(db, "users"),
  //       where("email", "==", email)
  //     );
  //     const unsubscribe = onSnapshot(queryUser, (querySnapshot) => {
  //       querySnapshot.forEach((user) => {
  //         userData = { ...user.data(), id: user.id };
  //         userId = user.id;
  //       });
  //     });

  //     unsubscribe();
  //     return NextResponse.json(
  //       {
  //         userData,
  //         userId: `user id is ${userId}`,
  //         message: "Fetched user data!",
  //       },
  //       {
  //         status: 200,
  //       }
  //     );
  //   } catch (error) {
  //     return NextResponse.json(
  //       {
  //         // @ts-ignore
  //         error: error.message,
  //         message: "Unable to get user data!",
  //       },
  //       { status: 500 }
  //     );
  //   }
}
