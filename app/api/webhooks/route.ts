import { NextRequest, NextResponse } from "next/server";
import Stripe from "stripe";
import postCheckout from "../../utils/postCheckout";
import postSubscription from "../../utils/postSubscription";
import postUnsubscribe from "../../utils/postUnsubscribe";
import postUpdate from "../../utils/postUpdate";

const stripe = new Stripe(process.env.NEXT_PUBLIC_STRIPE_KEY!, {
  apiVersion: "2023-10-16",
});

const endpointSecret = process.env.NEXT_PUBLIC_WEBHOOKS_SECRET!;

export const POST = async (req: NextRequest) => {
  try {
    const buf = await req.text();
    const sig = req.headers.get("stripe-signature")!;
    let event: Stripe.Event;

    try {
      event = stripe.webhooks.constructEvent(buf, sig, endpointSecret);
    } catch (error) {
      return NextResponse.json({
        // @ts-ignore
        error: error.message,
        status: 400,
      });
    }

    switch (event.type) {
      case "payment_intent.succeeded":
        const paymentIntentSucceeded = event.data.object;
        console.log(paymentIntentSucceeded);
        if (paymentIntentSucceeded.status === "succeeded") {
          return NextResponse.json({
            task: "succeeded",
          });
        }
        break;
      case "payment_intent.created":
        return NextResponse.json({
          task: "finished payment intent creation",
        });
      case "checkout.session.completed":
        const checkoutSucceeded = event.data.object;
        const session = await stripe.checkout.sessions.retrieve(
          checkoutSucceeded.id,
          { expand: ["line_items"] }
        );
        const sessionLineItems = await stripe.checkout.sessions.listLineItems(
          checkoutSucceeded.id
        );
        await postCheckout({ session, sessionLineItems });
        return NextResponse.json({
          msg: "finished task",
        });
      case "charge.succeeded":
        return NextResponse.json({
          task: "finished charge",
        });
      case "checkout.session.expired":
        return NextResponse.json({
          task: "session expired",
        });
      case "customer.subscription.created":
        const subscribed = event.data.object;
        await postSubscription(subscribed);
        return NextResponse.json({
          task: "subscribed",
        });
      case "customer.subscription.deleted":
        const unsubscribed = event.data.object;
        await postUnsubscribe(unsubscribed);
        break;
      case "customer.subscription.updated":
        const updated = event.data.object;
        await postUpdate(updated);
        return NextResponse.json({
          task: "updated",
        });

      // ... handle other event types
      default:
        console.log(`Unhandled event type ${event.type}`);
    }
  } catch (error) {
    return NextResponse.json({
      // @ts-ignore
      error: error.message,
      status: 400,
    }).headers.set("Allow", "POST");
  }
};
