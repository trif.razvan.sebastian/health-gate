import { NextRequest, NextResponse } from "next/server";
import Stripe from "stripe";
import { ESubscriptionActions } from "../../interfaces/addSubscriptionInterface";

export async function POST(req: NextRequest) {
  const stripe = new Stripe(process.env.NEXT_PUBLIC_STRIPE_KEY!);
  const data = await req.json();
  try {
    const session = await stripe.checkout.sessions.create({
      // @ts-ignore
      line_items: [
        {
          price_data: {
            currency: "ron",
            product_data: {
              name: data.title,
            },
            unit_amount: data.price,
            recurring: {
              interval: data.interval,
              interval_count: data.intervalCount,
            },
          },
          quantity: 1,
        },
      ],
      subscription_data: {
        metadata: {
          user_id: data.userId,
          sub_id: data.subId,
          discount_id: data.discountId,
          action: ESubscriptionActions.CREATE,
        },
      },
      automatic_tax: {
        enabled: true,
      },
      mode: "subscription",
      success_url: `http://localhost:3000`,
      cancel_url: `http://localhost:3000/subscriptions`,
      locale: "ro",
    });

    return NextResponse.json(session.url);
  } catch (error) {
    console.log(error);
    // @ts-ignore
    console.log(error.message);
    return NextResponse.json(
      // @ts-ignore
      { error: error.message },
      { status: 500 }
    );
  }
}
