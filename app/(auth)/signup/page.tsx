"use client";

import Link from "next/link";
import AuthForm from "../../components/AuthForm";
import { Center, Text } from "@mantine/core";
import createUser from "../../utils/createUser";
import ReturnHome from "../components/ReturnHome";

export default function Signup() {
  return (
    <>
      <ReturnHome />
      <Center h="95vh">
        <AuthForm
          alternativeRoute={
            <Text c="var(--text-color)">
              Ai deja cont?{" "}
              <Link style={{ color: "var(--primary-color)" }} href="/login">
                Logheaza-te!
              </Link>
            </Text>
          }
          title="Inregistreaza-te"
          isSignup
          onSubmitFunc={createUser}
        />
      </Center>
    </>
  );
}
