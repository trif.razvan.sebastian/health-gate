import { Flex } from "@mantine/core";
import Link from "next/link";

export default function ReturnHome() {
  return (
    <Flex p="5px 5px 0 0" justify="flex-end">
      <Link
        style={{
          color: "var(--text-color)",
          textDecoration: "none",
          fontSize: 10,
        }}
        href="/"
      >
        Sau inapoi acasa..
      </Link>
    </Flex>
  );
}
