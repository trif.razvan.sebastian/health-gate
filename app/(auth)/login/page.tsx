"use client";

import { Center, Text } from "@mantine/core";
import AuthForm from "../../components/AuthForm";
import Link from "next/link";
import loginUser from "../../utils/loginUser";
import ReturnHome from "../components/ReturnHome";

export default function Login() {
  return (
    <>
      <ReturnHome />
      <Center h="95vh">
        <AuthForm
          title="Logheaza-te"
          alternativeRoute={
            <Text c="var(--text-color)">
              Nu ai cont?{" "}
              <Link style={{ color: "var(--primary-color)" }} href="/signup">
                Creaza unul!
              </Link>
            </Text>
          }
          onSubmitFunc={loginUser}
        />
      </Center>
    </>
  );
}
