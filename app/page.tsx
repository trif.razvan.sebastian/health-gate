"use client";

import { Space } from "@mantine/core";
import CategoriesHighlights from "./components/home/CategoriesHighlights";
import Hero from "./components/home/Hero";
import IconsGroup from "./components/home/IconsGroup";
import MenuHighlights from "./components/home/MenuHighlights";
import SubscriptionHighlight from "./components/home/SubscriptionHighlight";

export default function Home() {
  return (
    <div>
      <Hero />
      <Space h="6rem" />
      <div className="margin-container">
        <IconsGroup />
      </div>
      <img className="peppers-img" src="/peppers.svg" alt="" />
      <Space h="6rem" />
      <div className="margin-container-left">
        <MenuHighlights />
      </div>
      <img className="bananas-img" src="/bananas.svg" alt="" />
      <Space h="6rem" />
      <div className="margin-container">
        <CategoriesHighlights />
      </div>
      <Space h="6rem" />
      <SubscriptionHighlight />
    </div>
  );
}
