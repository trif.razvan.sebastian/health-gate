import { Button, FileInput, Flex } from "@mantine/core";
import { useState } from "react";
import pushMenuToCloud from "../../utils/adminConsole/pushMenuToCloud";

export default function AddMenuItemsForm() {
  const [images, setImages] = useState<File[] | []>([]);

  return (
    <div>
      <p>Adauga in meniu</p>
      <Flex mb="lg" align="flex-end" gap="1rem">
        <FileInput
          size="xs"
          label="Imagini"
          w="20rem"
          multiple
          onChange={setImages}
        />
        <Button
          variant="light"
          color="green"
          size="xs"
          onClick={() => pushMenuToCloud({ images })}
        >
          Upload
        </Button>
      </Flex>
    </div>
  );
}
