import { Button, Select } from "@mantine/core";
import useAddMenuOfTheDay from "../../templates/adminConsole/useAddMenuOfTheDay";
import useGetMenu from "../../hooks/firebase/useGetMenu";
import addMenuOfTheDay from "../../utils/adminConsole/addMenuOfTheDay";
import MenuItemSelect from "./MenuItemSelect";

const days = [
  { label: "Lu", value: "monday" },
  { label: "Ma", value: "tuesday" },
  { label: "Mi", value: "wednesday" },
  { label: "Jo", value: "thursday" },
  { label: "Vi", value: "friday" },
];

export default function CreateMenuOfTheDayForm() {
  const { menu } = useGetMenu();
  const { addMenuOfTheDayTemplate } = useAddMenuOfTheDay();
  return (
    <div>
      <p>Adauga meniul zilei</p>
      <MenuItemSelect
        field="menuItemOne"
        menu={menu}
        addMenuOfTheDayTemplate={addMenuOfTheDayTemplate}
      />
      <br />
      <MenuItemSelect
        field="menuItemTwo"
        menu={menu}
        addMenuOfTheDayTemplate={addMenuOfTheDayTemplate}
      />
      <br />
      <Select
        w="20rem"
        {...addMenuOfTheDayTemplate.getInputProps("forDay")}
        label="Alege ziua"
        data={days}
      />
      <br />
      <Button
        variant="light"
        color="green"
        size="xs"
        onClick={() => addMenuOfTheDay(addMenuOfTheDayTemplate.values)}
      >
        Creaza meniu
      </Button>
    </div>
  );
}
