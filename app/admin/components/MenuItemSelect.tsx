import { NativeSelect, Select } from "@mantine/core";
import { IMenu } from "../../interfaces/menuInterface";
import { FormEvent, useState, ChangeEvent } from "react";
import { IAddMenuOfTheDay } from "../../interfaces/menuOfTheDayInterface";
import { UseFormReturnType } from "@mantine/form";
interface IMenuSelect {
  menu: IMenu[];
  addMenuOfTheDayTemplate: UseFormReturnType<
    IAddMenuOfTheDay,
    (values: IAddMenuOfTheDay) => IAddMenuOfTheDay
  >;
  field: string;
}

export default function MenuItemSelect({
  menu,
  addMenuOfTheDayTemplate,
  field,
}: IMenuSelect) {
  const selectData = menu?.map((item) => {
    return {
      label: item.title as string,
      value: `${item.id}/${item.price}` as string,
    };
  });

  const selectItem = (e: ChangeEvent<HTMLSelectElement>) => {
    addMenuOfTheDayTemplate.setFieldValue(field, e);
  };

  return (
    <Select
      searchable
      clearable
      w="20rem"
      label="Alege din lista"
      data={selectData}
      //   @ts-ignore
      onChange={(e) => selectItem(e)}
    />
  );
}
