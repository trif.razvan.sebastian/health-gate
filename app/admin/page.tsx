"use client"

import AddMenuItemsForm from "./components/AddMenuItemsForm"
import CreateMenuOfTheDayForm from "./components/CreateMenuOfTheDayForm"

export default function AdminConsole() {
  return (
    <div>
        <p>admin</p>
        <CreateMenuOfTheDayForm />
        <br />
        <AddMenuItemsForm />
    </div>
  )
}
