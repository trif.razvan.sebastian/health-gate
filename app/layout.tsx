import "@mantine/core/styles.css";
import '@mantine/carousel/styles.css';
import type { Metadata } from "next";
import { MantineProvider, ColorSchemeScript } from "@mantine/core";
import { theme } from "../theme";
import NavigationLayout from "./components/NavigationLayout";
import "./globals.css";
import { Notifications } from "@mantine/notifications";
import { ModalsProvider } from "@mantine/modals";
import { Poppins, Young_Serif } from "next/font/google";

export const metadata: Metadata = {
  title: "Health Gate",
  description: "Your reliable source for healthy bio food",
};

const poppins = Poppins({
  subsets: ["latin"],
  weight: ["400", "700", "200", "600"],
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
      <link rel="icon" href="/favicon.svg" sizes="any" />
        <ColorSchemeScript />
      </head>
      <body className={poppins.className}>
        <MantineProvider defaultColorScheme="light" theme={theme}>
          <ModalsProvider>
            <NavigationLayout>
              {children}
              <Notifications />
            </NavigationLayout>
          </ModalsProvider>
        </MantineProvider>
      </body>
    </html>
  );
}
