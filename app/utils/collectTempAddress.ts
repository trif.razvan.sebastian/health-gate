import { addDoc, collection } from "firebase/firestore";
import { db } from "../firebase/firebase";
import { INewCheckout } from "../interfaces/checkoutInterface";

interface ICollectTempAddressParams {
  form: INewCheckout;
  userId: string | undefined;
}

export default async function collectTempAddress({
  form,
  userId,
}: ICollectTempAddressParams) {
  try {
    const docRef = await addDoc(
      collection(db, `users/${userId as string}/tempAddresses`),
      {
        line1: form.customer.address.line1,
        line2: form.customer.address.line2,
        city: form.customer.address.city,
        county: form.customer.address.county,
        postalCode: form.customer.address.postalCode,
      }
    );
    const addressId = docRef.id
    return addressId
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  } finally {
    console.log("address stored");
  }
}
