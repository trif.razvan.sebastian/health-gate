import { doc, getDoc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";
import Stripe from "stripe";
import moment from "moment";
import { ESubscriptionActions } from "../interfaces/addSubscriptionInterface";

export default async function postUpdate(updated: Stripe.Subscription) {
  const action = updated.metadata.action;
  if (action !== ESubscriptionActions.UPDATE) return;
  try {
    // const thresholdDuration = moment.duration(20, "seconds");
    // const isSubscriptionNewlyCreated =
    // moment().diff(moment.unix(updated.created), "seconds") <=
    // thresholdDuration.asSeconds();
    
    // if (isSubscriptionNewlyCreated) {
    //   console.log("New subscription created. No update needed.");
    //   return;
    // }
    
    const dateNow = moment().format("DD.MM.YYYY, HH:mm:ss");
    const docRef = doc(db, "users", updated.metadata.user_id);
    const docSnapshot = await getDoc(docRef);
    const curentDocState = docSnapshot.data();
    await updateDoc(docRef, {
      activeSubscription: {
        ...curentDocState?.activeSubscription,
        period: {
          ...curentDocState?.activeSubscription.period,
          update: dateNow,
        },
        cancelAtPeriodEnd: true,
      },
    });
  } catch (error) {
    console.log(error);
  }
}
