import { deleteDoc, doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

interface IUpdateCartQuantityParams {
  userId: string;
  cartId: string | undefined;
  quantity: number | string;
}

export default async function updateCartQuantity({
  userId,
  cartId,
  quantity,
}: IUpdateCartQuantityParams) {
  const docRef = doc(db, `users/${userId}/cart`, cartId as string);
  try {
    await updateDoc(docRef, {
      productQuantity: +quantity,
    }).then(async () => {
      if (+quantity > 0) {
        console.log("updated q");
        return;
      } else if (+quantity === 0) {
        await deleteDoc(doc(db, `users/${userId}/cart`, cartId as string));
        console.log("deleted doc");
      }
    });
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
