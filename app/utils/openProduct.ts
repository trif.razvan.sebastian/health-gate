import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import { ReadonlyURLSearchParams } from "next/navigation";


export default function openProduct(
    val: string | null,
    router: AppRouterInstance,
    searchParams: ReadonlyURLSearchParams
) {
    const params = new URLSearchParams(searchParams);
    if (val) {
      params.set("product", val);
    } else {
      params.delete("product");
    }
    router.replace(`/menu?${params.toString()}`, { scroll: false });
}
