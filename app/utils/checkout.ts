import axios from "axios";
import collectTempAddress from "./collectTempAddress";
import { INewCheckout } from "../interfaces/checkoutInterface";
import { ICartItems } from "../interfaces/cartInterface";

interface ICheckout {
  cartItems?: ICartItems[];
  userId: string | undefined;
  form: INewCheckout;
  ordersCount: number | undefined;
  lastMenuOrder?: string;
  isActive?: boolean;
}

export default async function checkout({
  cartItems,
  userId,
  form,
  ordersCount,
  isActive,
  lastMenuOrder,
}: ICheckout) {
  const addressId = await collectTempAddress({ userId, form });
  const fullName = form.customer.fullName
  try {
    const { data } = await axios.post(
      "/api/payment",
      {
        cartItems: cartItems,
        userId,
        addressId,
        ordersCount,
        fullName,
        phoneNumber: form.customer.phone,
        isActive,
        lastMenuOrder,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    window.location.assign(data);
    // const res = await fetch("/api/payment", {
    //   method: "POST",
    //   headers: {
    //     "Content-Type": "application/json",
    //   },
    //   body: JSON.stringify({ cartTotal: formated}),
    // })
    // .then(async (response) => {
    //     if (response.ok) {
    //       return response;
    //     } else {
    //         const errorMessage = await res.text();
    //         console.error("Error while sending payload:", errorMessage);
    //         throw new Error(errorMessage);        }
    //   })
    //   .catch((error) => {
    //     throw new Error("error while sending payload");
    //   });
  } catch (error) {
    console.log(error);
  }
}
