import {
  addDoc,
  collection,
  doc,
  increment,
  updateDoc,
} from "firebase/firestore";
import { db } from "../firebase/firebase";
import { ICart } from "../interfaces/cartInterface";
import { IAuth } from "../interfaces/authInterface";
import moment from "moment";

interface IAddToCartParams {
  productId: string | undefined;
  price: number;
  quantity?: number; //not needed
  userId: string;
  image?: string;
  title?: string;
  realtimeCart: ICart[];
  menuItems?: (string | undefined)[];
  userData?: IAuth;
  isActive: boolean;
  lastMenuOrder: string;
  event?: React.MouseEvent
}

export default async function addToCart({
  productId,
  price,
  userId,
  quantity,
  image,
  title,
  realtimeCart,
  menuItems,
  userData,
  isActive,
  lastMenuOrder,
  event
}: IAddToCartParams) {
  event?.stopPropagation()
  const today = moment().format("DD.MM.YYYY")
  // const today = moment().format("dddd");
  const isFreeMenu =
    today !== lastMenuOrder &&
    isActive &&
    menuItems?.length &&
    !realtimeCart.find((item) => item.productPrice === 0);

  const existingCartItem = realtimeCart.find((item) => item.productId === productId);
  const existingRegularCartItem = realtimeCart.find((item) => item.productId === productId && item.productPrice > 0)

  try {
    if (!existingCartItem || (existingCartItem.productPrice === 0 && !existingRegularCartItem?.productId)) {
      if (isFreeMenu) {
        // If it's a free menu and not in the cart, add a new free menu
        await addDoc(collection(db, `users/${userId}/cart`), {
          productPrice: 0,
          productId,
          productQuantity: quantity || 1,
          productImage: image,
          productTitle: title,
          menuItems: menuItems || [],
        });
        console.log("Free menu added");
      } else {
        // If it's not a free menu and not in the cart, add a regular product
        await addDoc(collection(db, `users/${userId}/cart`), {
          productPrice: price * 1,
          productId,
          productQuantity: quantity || 1,
          productImage: image,
          productTitle: title,
          menuItems: menuItems || [],
        });
        console.log("Product added");
      }
    } else if(existingCartItem.productPrice > 0) {
      // If the existing item is not a free menu, update its quantity
      const docRefUpdate = doc(db, `users/${userId}/cart`, existingRegularCartItem?.id as string);
      await updateDoc(docRefUpdate, {
        productQuantity: increment(quantity || 1),
      });
      console.log("Quantity updated");
    }
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
