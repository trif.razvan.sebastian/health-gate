import { signOut } from "firebase/auth";
import { auth } from "../firebase/firebase";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";

const logoutUser = (router: AppRouterInstance) => {
  try {
    signOut(auth).then(() => {
      console.log("success logging out");
      router.push("/");
    });
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
};

export default logoutUser;
