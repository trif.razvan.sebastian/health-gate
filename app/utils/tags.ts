export default function tags(
  paymentMehtod: string,
  hasFreeMenu: boolean,
  transport: number,
) {
  const tempTags = [paymentMehtod];
  if (transport === 0) {
    tempTags.push("TRANSPORT GRATUIT");
  }
  if (hasFreeMenu) {
    tempTags.push("MENIU GRATUIT");
  }
  return tempTags;
}
