import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { db, storage } from "../../firebase/firebase";
import { addDoc, collection } from "firebase/firestore";
import { IMenu } from "../../interfaces/menuInterface";
const dummy = [
  {
    title: "",
    description: "",
    price: 0,
    weight: 0,
    nutritionalValues: {
      calories: "kcal",
      carbohydrates: "g",
      proteins: "g",
      fats: "g",
      fibers: "g",
    },
    image: ".webp",
    category: "soup",
  },
];

export default async function pushMenuToCloud({
  images,
}: {
  images: File[] | [];
}) {
  const tempData: IMenu[] = [];
  try {
    await Promise.all(
      dummy.map(async (obj, i) => {
        const { image, ...rest } = obj;
        const filteredImage = images
          .filter((img) => img.name === obj.image)
          .shift();
        const modifiedName = filteredImage?.name.replace(/ /g, "_");
        const storageRef = ref(storage, `/menu/${modifiedName}`);
        try {
          await uploadBytes(storageRef, filteredImage as File);
          const downloadURL = await getDownloadURL(storageRef);
          tempData.push({ ...rest, image: downloadURL });
          console.log("1...got the link and pushed to tempData");
        } catch (error) {
          // @ts-ignore
          console.log(error.message);
        }
      })
    );
    try {
      await Promise.all(
        tempData.map(async (obj) => {
          const docRef = await addDoc(collection(db, "menu"), obj);
        })
      );
      console.log("2...pushed the object to menu collection");
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  } finally {
    console.log("FINISHED JOB");
  }
}
