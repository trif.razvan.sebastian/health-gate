import { addDoc, collection } from "firebase/firestore";
import moment from "moment";
import { db } from "../../firebase/firebase";
import { IAddMenuOfTheDay } from "../../interfaces/menuOfTheDayInterface";

export default async function addMenuOfTheDay(values: IAddMenuOfTheDay) {
  const momentDate = moment().format("DD.MM.YYYY, hh:mm:ss");
  const { menuItemOne, menuItemTwo, forDay } = values;
  const prices = [menuItemOne, menuItemTwo].map((item) => {
    return +item.split("/")[1];
  });
  const menuTotal = prices.reduce((a, b) => a + b);
  try {
    const docRef = await addDoc(collection(db, "menuOfTheDay"), {
      // add title property
      menuItemOne: menuItemOne.split("/")[0],
      menuItemTwo: menuItemTwo.split("/")[0],
      menuTotal,
      createdAt: momentDate,
      updatedAt: "",
      forDay,
    });
    console.log("added menu")
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}


// friptura de vita - inghetata
//  orez cu pui - supa crema de dovleac
// burger - salata quatro