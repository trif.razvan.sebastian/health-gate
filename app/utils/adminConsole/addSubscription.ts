import { addDoc, collection } from "firebase/firestore";
import { db } from "../../firebase/firebase";
import moment from "moment";
import { IAddSubscription } from "../../interfaces/addSubscriptionInterface";
import { UseFormReturnType } from "@mantine/form";

interface IAddFunction {
  values: IAddSubscription;
  addSubscriptionTemplate: UseFormReturnType<IAddSubscription>;
}

export default async function addSubscription({
  values,
  addSubscriptionTemplate,
}: IAddFunction) {
  const momentDate = moment().format("DD.MM.YYYY, hh:mm:ss");
  const { ...rest } = values;
  try {
    const docRef = await addDoc(collection(db, "subscriptions"), {
      ...rest,
      createdAt: momentDate,
    });
    console.log("added subscription");
    addSubscriptionTemplate.reset();
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
