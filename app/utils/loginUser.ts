import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from "../firebase/firebase";
import { IAuth, ILogin } from "../interfaces/authInterface";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";

const loginUser = async (values: ILogin, router: AppRouterInstance) => {
  const { email, password } = values;
  signInWithEmailAndPassword(auth, email, password as string).then((userCredentials) => {
    try {
      router.replace("/my-account")
      console.log(userCredentials, "success loging in");
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
  });
};

export default loginUser;
