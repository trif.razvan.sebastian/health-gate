import { doc, updateDoc } from "firebase/firestore";
import moment from "moment";
import Stripe from "stripe";
import { db } from "../firebase/firebase";

export default async function postUnsubscribe(
  unsubscribed: Stripe.Subscription
) {

  try {
    const dateNow = moment().format("DD.MM.YYYY, HH:mm:ss");
    const docRef = doc(db, "users", unsubscribed.metadata.user_id);
    await updateDoc(docRef, {
      activeSubscription: {
        subscriptionId: "",
        period: {
          end: dateNow,
          update: dateNow,
        },
        subscriptionDiscountAmount: 0,
        isActive: false,
        stripeSubscriptionId: "",
        cancelAtPeriodEnd: false,
      },
    });
  } catch (error) {
    console.log(error);
  }
}
