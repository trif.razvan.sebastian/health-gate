import axios from "axios";

interface ISubscribe {
    title: string;
    price: number;
    interval: string;
    intervalCount: number
    userId: string
    id: string;
    discountId: string
}

export default async function subscribe({title, price, interval, intervalCount, userId, id, discountId}: ISubscribe) {
      try {
    const { data } = await axios.post(
      "/api/subscription",
      {
        title,
        price: price*100,
        interval,
        intervalCount,
        userId,
        subId: id,
        discountId
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    window.location.assign(data)
  } catch (error) {
    console.log(error)
  }
}
