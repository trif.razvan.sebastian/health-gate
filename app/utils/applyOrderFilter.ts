import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import { ReadonlyURLSearchParams } from "next/navigation";

export default function applyOrderFilter(
  val: string,
  router: AppRouterInstance,
  searchParams: ReadonlyURLSearchParams
) {
  const params = new URLSearchParams(searchParams);
  if (val && !params.get(val.toLowerCase())) {
    params.set(val.toLowerCase(), val);
  } else if (params.get(val.toLowerCase())) {
    params.delete(val.toLowerCase());
  } else {
    params.delete(val.toLowerCase());
  }
  router.replace(`/my-account?${params.toString()}`, { scroll: false });
}
