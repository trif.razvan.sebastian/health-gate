import { IAuth } from "../interfaces/authInterface";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import axios from "axios";

const createUser = async (values: IAuth, router: AppRouterInstance) => {
  try {
    const { data } = await axios.post(
      "/api/user",
      {
        values,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  } finally {
    router.push("/my-account");
  }
};

export default createUser;
