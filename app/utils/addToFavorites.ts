import { arrayUnion, doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

export default async function addToFavorites(
  userId: string,
  productId: string,
  favorites: string[]
) {
  const docref = doc(db, "users", userId as string);
  if (favorites.includes(productId)) return;
  else {
      try {
        await updateDoc(docref, {
          favorites: arrayUnion(productId as string),
        });
      } catch (error) {
        console.log(error);
      }
  }
}
