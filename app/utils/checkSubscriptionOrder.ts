import moment from "moment";
import { ICartItems } from "../interfaces/cartInterface";

interface ICheckSubscriptionOrder {
  cartItems: ICartItems[];
  lastMenuOrder?: string;
  isActive?: boolean;
}

export default function checkSubscriptionOrder({
  cartItems,
  lastMenuOrder,
  isActive,
}: ICheckSubscriptionOrder) {
  // verify if menu from cartitems if it has menuItems or something
  // ??
  // return the new cartItems with the prices modified if conditions are met

  try {
    if (!isActive) return;
    const yesterday = moment().subtract(1, "day");
    let hasFreeMenu = false
    const today = moment().format("DD.MM.YYYY")
    const modifiedCartItems = cartItems.map((item) => {
      let priceAfterCheck;
      let quantityAfterCheck;
      if (today !== lastMenuOrder && isActive && item.menuItems?.length) {
        priceAfterCheck = 0;
        hasFreeMenu = true
        if (item.quantity > 1) {
          quantityAfterCheck = item.quantity - 1;
          priceAfterCheck = undefined;
        }
      }

      return {
        ...item,
        price: priceAfterCheck !== undefined ? priceAfterCheck : item.price,
        quantity:
          quantityAfterCheck !== undefined ? quantityAfterCheck : item.quantity,
      };
    });
    return { modifiedCartItems, hasFreeMenu};
  } catch (error) {
    console.log(error);
  }
}
