import { doc, updateDoc } from "firebase/firestore";
import moment from "moment";
import Stripe from "stripe";
import { db } from "../firebase/firebase";
import { ESubscriptionActions } from "../interfaces/addSubscriptionInterface";

export default async function postSubscription(sub: Stripe.Subscription) {
  const action = sub.metadata.action;
  if (action !== ESubscriptionActions.CREATE) return;
  try {
    const dateNow = moment().format("DD.MM.YYYY, HH:mm:ss");
    const dateEnd = moment
      .unix(sub.current_period_end)
      .format("DD.MM.YYYY, HH:mm:ss");

    // move this to a separete function createDiscountAtSubscription and use the function inside the subscription route
    // let promoCodeData = null;
    // try {
    //   const { data } = await axios.post(
    //     "api/promo-code",
    //     {
    //       couponId: sub.metadata.discount_id,
    //       dateEnd: sub.current_period_end,
    //     },
    //     {
    //       headers: { "Content-Type": "application/json" },
    //     }
    //   );
    //   promoCodeData = data.promoCode;
    // } catch (error) {
    //   // @ts-ignore
    //   console.log(error.message);
    // }

    const docRef = doc(db, "users", sub.metadata.user_id as string);
    await updateDoc(docRef, {
      activeSubscription: {
        subscriptionId: sub.metadata.sub_id,
        period: {
          start: dateNow,
          end: dateEnd,
          update: dateNow,
        },
        subscriptionDiscountAmount:
          (sub.items.data[0].plan.amount as number) / 100,
        isActive: true,
        stripeSubscriptionId: sub.id,
        cancelAtPeriodEnd: false,
        discountId: sub.metadata.discount_id,
        // discountId: promoCodeData.code || "didnt work??",
      },
    });
  } catch (error) {
    console.log(error);
  }
}
