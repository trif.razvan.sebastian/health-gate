import { modals } from "@mantine/modals";

interface IConfirmModalProps {
  title?: string;
  children: string | React.ReactNode;
  onConfirm: () => void;
  onCancel?: () => void;
}

const customConfirmModal = ({
  title,
  children,
  onConfirm,
  onCancel,
}: IConfirmModalProps) =>
  modals.openConfirmModal({
    title: title,
    children: children,
    labels: { confirm: "Confirma", cancel: "Anulare" },
    onCancel: () => onCancel && onCancel(),
    onConfirm: () => onConfirm(),
  });

export default customConfirmModal;
