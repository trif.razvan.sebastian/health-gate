import { doc, getDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

interface IReturnTempAddress {
  addressId: string | undefined;
  userId: string | undefined;
}

export default async function returnTempAddress({
  addressId,
  userId,
}: IReturnTempAddress) {
  try {
    const docRef = doc(
      db,
      `users/${userId}/tempAddresses`,
      addressId as string
    );
    const docSnap = await getDoc(docRef);

    if (docSnap.exists()) {
      return { ...docSnap.data() };
    }
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
