import { doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

export default async function removeFromFavorites(
  userId: string,
  productId: string,
  favorites: string[]
) {
  const docref = doc(db, "users", userId as string);
  if (!favorites.includes(productId)) return;
  else {
    try {
      const newTempArray = favorites.filter((item) => item !== productId);
      await updateDoc(docref, {
        favorites: newTempArray,
      });
    } catch (error) {
      console.log(error);
    }
  }
}
