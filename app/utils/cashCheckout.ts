import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  increment,
  updateDoc,
} from "firebase/firestore";
import moment from "moment";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import { db } from "../firebase/firebase";
import { ICartItems } from "../interfaces/cartInterface";
import { INewCheckout } from "../interfaces/checkoutInterface";
import calculateTransport from "./calculateTransport";
import checkOrdersStreak from "./checkOrdersStreak";
import checkSubscriptionOrder from "./checkSubscriptionOrder";
import tags from "./tags";

// maybe make a route to do all this and use the function to call the route
// redirect the user and clear the form after checkout
export default async function cashCheckout(
  form: INewCheckout,
  ordersCount: number | undefined,
  userId: string | undefined,
  cartItems: ICartItems[],
  lastMenuOrder?: string,
  isActive?: boolean,
  router?: AppRouterInstance
) {
  try {
    const checked = checkSubscriptionOrder({
      isActive,
      lastMenuOrder,
      cartItems,
    });
    const checkoutTotals = calculateTransport(isActive as boolean, cartItems);
    const subtotal = checkoutTotals.cartSubtotal;
    const tax = subtotal * 0.19;
    const total = subtotal - form.discount + checkoutTotals.transportTotal;
    const today = moment().format("DD.MM.YYYY");

    const docRef = await addDoc(collection(db, "orders"), {
      orderId: (ordersCount as number) + 1,
      customer: {
        customerId: userId as string,
        fullName: form.customer.fullName,
        email: form.customer.email,
        phone: form.customer.phone,
        address: {
          line1: form.customer.address.line1,
          line2: form.customer.address.line2,
          city: form.customer.address.city,
          county: form.customer.address.county,
          postalCode: form.customer.address.postalCode,
        },
      },
      items: cartItems?.map((prod) => ({
        productId: prod.id,
        productQuantity: prod.quantity,
        productCurrency: "ron",
        productPrice: prod.price / 100,
        cartId: prod.cartId,
        menuItems: (prod.menuItems?.length && prod.menuItems) || [],
      })),
      subtotal,
      tax,
      total,
      shipping: checkoutTotals.transportTotal,
      discount: form.discount,
      payment: {
        method: "CASH",
        paymentStatus: "UNPAID",
      },
      status: "COOKING",
      createdAt: moment().format("DD.MM.YYYY, HH:mm:ss"),
      updatedAt: moment().format("DD.MM.YYYY, HH:mm:ss"),
      sessionId: "",
      tags: tags(
        "CASH",
        checked?.hasFreeMenu as boolean,
        checkoutTotals.transportTotal
      ),
    });

    try {
      await Promise.all(
        cartItems.map(async (prod) => {
          await deleteDoc(
            doc(db, `users/${userId as string}/cart`, prod.cartId)
          );
        })
      );
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }

    try {
      const docRef = doc(db, "utils", process.env.NEXT_PUBLIC_UTILS_DOC!);
      await updateDoc(docRef, {
        ordersCount: increment(1),
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
    try {
      if (!checked?.hasFreeMenu) return;
      const docRefMenuOrderDayUpdate = doc(db, "users", userId as string);
      await updateDoc(docRefMenuOrderDayUpdate, {
        lastMenuOrder: today,
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
    checkOrdersStreak(userId as string);
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  } finally {
    router?.push("/");
    console.log("finished the operation");
  }
}
