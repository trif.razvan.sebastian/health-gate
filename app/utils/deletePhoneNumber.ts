import { doc, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

export default async function deletePhoneNumber(
  id: string | undefined,
  i: number,
  phoneNumbers: string[],
) {
  const docRef = doc(db, "users", id as string);
  const newNumbersList = phoneNumbers
    ?.filter((item, index) => index !== i)
  try {
    await updateDoc(docRef, {
      phoneNumbers: newNumbersList,
    });
    console.log("remained", newNumbersList);
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
