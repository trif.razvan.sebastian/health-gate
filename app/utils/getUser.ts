import axios from "axios";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../firebase/firebase";

export default function getUser() {
  return new Promise(async (resolve, reject) => {
    onAuthStateChanged(auth, async (user) => {
      if (user) {
        try {
          const email = user.email;

          try {
            const { data } = await axios.get("/api/user", {
              params: {
                email: email,
              },
            });

            resolve(data);
          } catch (error) {
            // @ts-ignore
            reject(error.message);
          }
        } catch (error) {
          // @ts-ignore
          reject(error.message);
        }
      }
    });
  });
}
