import { AppRouterInstance } from "next/dist/shared/lib/app-router-context.shared-runtime";
import { ReadonlyURLSearchParams } from "next/navigation";

export const goToCategory = (
  val: string,
  router: AppRouterInstance,
  searchParams: ReadonlyURLSearchParams
) => {
  const params = new URLSearchParams(searchParams);
  if (val) {
    params.set("category", val);
  } else {
    params.delete("category");
  }
  router.replace(`/menu?${params.toString()}`, { scroll: false });
};
