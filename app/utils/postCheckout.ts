import {
  addDoc,
  collection,
  deleteDoc,
  doc,
  increment,
  updateDoc,
} from "firebase/firestore";
import moment from "moment";
import Stripe from "stripe";
import { db } from "../firebase/firebase";
import checkOrdersStreak from "./checkOrdersStreak";
import returnTempAddress from "./returnTempAddress";
import tags from "./tags";

interface IPostCheckout {
  session: Stripe.Checkout.Session;
  sessionLineItems: Stripe.Response<Stripe.ApiList<Stripe.LineItem>>;
}

export default async function postCheckout({
  session,
  sessionLineItems,
}: IPostCheckout) {
  // @ts-ignore
  const jsonIdsArr = JSON.parse(session.metadata.product_ids);
  const userId = session.metadata?.user_id;
  const addressId = session.metadata?.address_id;
  const address = await returnTempAddress({ addressId, userId });
  // @ts-ignore
  const jsonMenuItemsArr = JSON.parse(session.metadata.menu_items);
  const today = moment().format("DD.MM.YYYY");
  const hasFreeMenu = JSON.parse(session.metadata?.has_free_menu as string);
  try {
    const docRef = await addDoc(collection(db, "orders"), {
      orderId: Number(session.metadata?.orders_count) + 1,
      customer: {
        customerId: session.metadata?.user_id,
        fullName: session.metadata?.full_name,
        email: session.customer_details?.email,
        phone: session.metadata?.phone,
        address: {
          line1: address?.line1,
          line2: address?.line1,
          city: address?.city,
          county: address?.county,
          postalCode: address?.postalCode,
        },
      },
      items: sessionLineItems?.data.map((prod, i) => ({
        productId: jsonIdsArr[i].split("/")[0],
        productQuantity: prod.quantity,
        productCurrency: prod.currency,
        productPrice: prod.amount_total / 100,
        cartId: jsonIdsArr[i].split("/")[1],
        menuItems: (jsonMenuItemsArr[i] && jsonMenuItemsArr[i]) || [],
      })),
      subtotal: session.amount_subtotal! / 100,
      tax: session.total_details?.amount_tax! / 100,
      total: session.amount_total! / 100,
      shipping: session.total_details?.amount_shipping! / 100 || 0,
      discount: session.total_details?.amount_discount! / 100 || 0,
      payment: {
        method: "CARD",
        paymentStatus: "PAID",
      },
      status: "COOKING",
      createdAt: moment().format("DD.MM.YYYY, HH:mm:ss"),
      updatedAt: moment().format("DD.MM.YYYY, HH:mm:ss"),
      sessionId: session.id || "",
      tags: tags(
        "CARD",
        hasFreeMenu as boolean,
        session.total_details?.amount_shipping! / 100
      ),
    });

    try {
      await Promise.all(
        sessionLineItems?.data.map(async (prod, i) => {
          await deleteDoc(
            doc(
              db,
              `users/${session.metadata?.user_id as string}/cart`,
              jsonIdsArr[i].split("/")[1]
            )
          );
        })
      );
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }

    try {
      await deleteDoc(
        doc(
          db,
          `users/${session.metadata?.user_id as string}/tempAddresses`,
          addressId as string // addresses dont get deleted, fix!!!
        )
      );
      const docRefUpdate = doc(db, "utils", process.env.NEXT_PUBLIC_UTILS_DOC!);
      await updateDoc(docRefUpdate, {
        ordersCount: increment(1),
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
    try {
      if (!hasFreeMenu) return;
      const docRefMenuOrderDayUpdate = doc(
        db,
        "users",
        session.metadata?.user_id as string
      );
      await updateDoc(docRefMenuOrderDayUpdate, {
        lastMenuOrder: today,
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
    checkOrdersStreak(userId as string);
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  } finally {
    console.log("finished the operation");
  }
}
