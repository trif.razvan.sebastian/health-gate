import moment from "moment";

export default function weekendButtonDisabled() {
  const weekendDays = ["saturday", "sunday"];
  const today = moment().format("dddd").toLowerCase();
  return weekendDays.includes(today);
}
