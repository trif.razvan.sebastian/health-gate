import axios from "axios";

export default async function getCart(id: string | undefined) {
  try {
    const { data } = await axios.get("/api/user/cart", {
      params: {
        id,
      },
    });
    return data;
  } catch (error) {
    // @ts-ignore
    console.log(error.message);
  }
}
