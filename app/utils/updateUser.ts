import { IAuth } from "../interfaces/authInterface";
import { User } from "firebase/auth";
import axios from "axios";

export default async function updateUser(
  id: string | undefined,
  values: IAuth,
  userData: IAuth | undefined,
  user: User | null
) {
  const { data } = await axios.patch(
    `/api/user/${id}`,
    {
      values,
      userData,
      user,
    },
    {
      headers: {
        "Content-Type": "application/json",
      },
    }
  );
}
