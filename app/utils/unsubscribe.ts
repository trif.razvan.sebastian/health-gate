import axios from "axios";

export default async function unsubscribe(stripeSubscriptionId: string) {
  try {
    await axios.get(`/api/cancel-subscription`, {
      params: {
        stripeSubscriptionId
      }
    });
  } catch (error) {
    //@ts-ignore
    console.log(error.message);
  }
}
