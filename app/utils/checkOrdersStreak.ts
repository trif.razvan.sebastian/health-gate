import { doc, increment, updateDoc } from "firebase/firestore";
import { db } from "../firebase/firebase";

export default async function checkOrdersStreak(
//   ordersStreak: number,
  userId: string
) {
    try {
      const docRef = doc(db, "users", userId);
      await updateDoc(docRef, {
        ordersStreak: increment(1),
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
  }
