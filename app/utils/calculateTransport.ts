import { ICartItems } from "../interfaces/cartInterface";

export default function calculateTransport(
  isActive: boolean,
  cartItems: ICartItems[]
) {
  let transportTotal = 1000;
  let cartDisplaySubtotal = 0;
  const cartSubtotal =
    cartItems.length &&
    cartItems
      ?.map((item) => item.price * item.quantity)
      .reduce((a, b) => a + b);

  if (isActive) {
    transportTotal = 0;
  } else if (cartSubtotal > 20000) {
    transportTotal = 0;
  }

  return {
    transportTotal: transportTotal / 100,
    formatedTransportTotal: transportTotal,
    cartTotal: (cartSubtotal + transportTotal) / 100,
    cartSubtotal: cartSubtotal / 100,
    cartDisplaySubtotal: cartSubtotal / 100 || cartDisplaySubtotal,
  };
}
