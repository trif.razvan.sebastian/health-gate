"use client";

import { Flex, Loader, TextInput } from "@mantine/core";
import { useRouter } from "next/navigation";
import { useState } from "react";
import CartProductsList from "../components/CartProductsList";
import EqualGapContainer from "../components/EqualGapContainer";
import NoWeekendDeliveryMessage from "../components/NoWeekendDeliveryMessage";
import useGetCart from "../hooks/firebase/useGetCart";
import useGetUserData from "../hooks/firebase/useGetUserData";
import useGetUtils from "../hooks/firebase/useGetUtils";
import classes from "../modules/Checkout.module.css";
import useCheckoutTemplate from "../templates/useCheckoutTemplate";
import calculateTransport from "../utils/calculateTransport";
import cashCheckout from "../utils/cashCheckout";
import checkout from "../utils/checkout";
import weekendButtonDisabled from "../utils/weekendButtonDisabled";
import AdressInfoForm from "./components/AdressInfoForm";
import GenerealInfoForm from "./components/GenerealInfoForm";

export default function Checkout() {
  const router = useRouter();
  const { realtimeCart } = useGetCart();
  const { userId, userData } = useGetUserData();
  const { checkoutFormTemplate } = useCheckoutTemplate();
  const { utilsObj } = useGetUtils();
  const form = checkoutFormTemplate.values;
  const ordersCount = utilsObj?.ordersCount;
  const [discountCode, setDiscountCode] = useState("");

  const cartItems = realtimeCart.map((prod) => ({
    price: prod.productPrice * 100,
    quantity: prod.productQuantity,
    title: prod.productTitle,
    image: prod.productImage,
    id: prod.productId,
    cartId: prod.id as string,
    menuItems: prod.menuItems as string[],
  }));

  // create an useeffect to redirect the user if the cart is empty
  //  display on the page the menu with 0 as total

  return realtimeCart.length ? (
    <>
      <img className={classes.bananasImgCheckout} src="/bananas.svg" alt="" />
      <img className={classes.peppersImgCheckout} src="/peppers.svg" alt="" />
      <div className="margin-container">
        <div className={classes.checkoutContainerFlex}>
          <EqualGapContainer>
            <div>
              <h4 className="h4-text">Date personale</h4>
              <GenerealInfoForm checkoutFormTemplate={checkoutFormTemplate} />
            </div>
            <div>
              <h4 className="h4-text">Date despre adresa</h4>
              <AdressInfoForm
                checkoutFormTemplate={checkoutFormTemplate}
                flexAdressLines
              />
            </div>
          </EqualGapContainer>
          <EqualGapContainer gap=".5rem">
            <h4 className="h4-text">Produsele din cos</h4>
            <CartProductsList realtimeCart={realtimeCart}/>
            <TextInput
              onChange={(e) => setDiscountCode(e.currentTarget.value)}
              label="Cod de reducere"
            />
            <Flex justify="space-between">
              <p className="p-text">Subtotal:</p>
              <p style={{ color: "var(--dark)" }} className="p-text">
                {calculateTransport(
                  userData?.activeSubscription.isActive as boolean,
                  cartItems
                )
                  .cartSubtotal.toLocaleString("ro-RO", {
                    style: "currency",
                    currency: "lei",
                  })
                  .toLowerCase()}
              </p>
            </Flex>

            <Flex justify="space-between">
              <p className="p-text">Transport:</p>
              <p style={{ color: "var(--dark)" }} className="p-text">
                {calculateTransport(
                  userData?.activeSubscription.isActive as boolean,
                  cartItems
                )
                  .transportTotal.toLocaleString("ro-RO", {
                    style: "currency",
                    currency: "lei",
                  })
                  .toLowerCase()}
              </p>
            </Flex>

            <Flex justify="space-between">
              <h4 style={{ color: "var(--text-color)" }} className="p-text">
                Total:
              </h4>
              <h4 className="h4-text">
                {calculateTransport(
                  userData?.activeSubscription.isActive as boolean,
                  cartItems
                )
                  .cartTotal.toLocaleString("ro-RO", {
                    style: "currency",
                    currency: "lei",
                  })
                  .toLowerCase()}
              </h4>
            </Flex>
            <p className="smaller-text">*Transport gratuit la comenzi de peste 200lei.</p>
            <p className="smaller-text">**Comenzile plasate dupa ora 15:00 o sa fie anulate.</p>
            {weekendButtonDisabled() ? (
              <NoWeekendDeliveryMessage />
            ) : (
              <Flex gap=".5rem" justify="space-between">
                <button
                  disabled={weekendButtonDisabled()}
                  onClick={() =>
                    checkout({
                      cartItems,
                      userId,
                      form,
                      ordersCount,
                      lastMenuOrder: userData?.lastMenuOrder,
                      isActive: userData?.activeSubscription?.isActive,
                    })
                  }
                  className="primary-button-accent"
                >
                  Plata cu cardul
                </button>
                {/* @ts-ignore */}
                <button
                  disabled={weekendButtonDisabled()}
                  onClick={() =>
                    cashCheckout(
                      form,
                      utilsObj?.ordersCount,
                      userId,
                      cartItems,
                      userData?.lastMenuOrder,
                      userData?.activeSubscription?.isActive,
                      router
                    )
                  }
                  className={"primary-button-dark"}
                >
                  Plata la livrare
                </button>
              </Flex>
            )}
          </EqualGapContainer>
        </div>
      </div>
    </>
  ) : (
    <Flex justify="center" align="center">
      <Loader size="lg" color="var(--primary-color)" />
    </Flex>
  );
}
