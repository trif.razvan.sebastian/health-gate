import { Group, Radio } from "@mantine/core";

export default function PaymentInfoForm() {
  return (
    // make the value modify the form template
    <Radio.Group value="cash" label="Alege modalitatea de plata">
      <Group mt="xs">
        <Radio color="var(--primary-color)" value="cash" label="Plata la livrare" />
        <Radio color="var(--primary-color)" value="card" label="Plata cu cardul" />
      </Group>
    </Radio.Group>
  );
}
