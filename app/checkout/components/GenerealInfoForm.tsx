
import {
  ActionIcon,
  Flex,
  NumberInput,
  Select,
  TextInput,
} from "@mantine/core";
import React from "react";
import useGetUserData from "../../hooks/firebase/useGetUserData";
import PersonIcon from "@mui/icons-material/Person";
import { UseFormReturnType } from "@mantine/form";
import { INewCheckout } from "../../interfaces/checkoutInterface";

export default function GenerealInfoForm( {checkoutFormTemplate}: {checkoutFormTemplate: UseFormReturnType<INewCheckout, (values: INewCheckout) => INewCheckout>} 
) {
  const { userData } = useGetUserData();

  const autocompleteField = (fieldName: string) => {
    // @ts-ignore
    checkoutFormTemplate.setFieldValue(`customer.${fieldName}`, userData?.[fieldName]);
  };

  return (
    <div>
      <Flex align="flex-end" gap="md">
        <TextInput
            {...checkoutFormTemplate.getInputProps("customer.fullName")}
          required
          w="100%"
          size="sm"
          label="Nume intreg"
        />
        <ActionIcon
          onClick={() => autocompleteField("fullName")}
          color="green"
          radius="20%"
          variant="light"
          size="lg"
        >
          <PersonIcon style={{ fontSize: 24 }} />
        </ActionIcon>
      </Flex>
      <Flex align="flex-end" gap="md">
        <TextInput
            {...checkoutFormTemplate.getInputProps("customer.email")}
          required
          w="100%"
          size="sm"
          label="Email"
        />
        <ActionIcon
          onClick={() => autocompleteField("email")}
          color="green"
          radius="20%"
          variant="light"
          size="lg"
        >
          <PersonIcon style={{ fontSize: 24 }} />
        </ActionIcon>
      </Flex>
      {userData?.phoneNumbers.length! ? (
        <Select
            {...checkoutFormTemplate.getInputProps("customer.phone")}
          required
          placeholder="Alege un numar de telefon"
          size="sm"
          label="Numar de telefon principal"
          data={userData?.phoneNumbers}
        />
      ) : (
        <NumberInput
            {...checkoutFormTemplate.getInputProps("customer.phone")}
            required={!userData?.phoneNumbers.length!}
          w="100%"
          size="sm"
          label="Numar de telefon"
          hideControls
        />
      )}
    </div>
  );
}
