import { TextInput } from "@mantine/core";
import { INewCheckout } from "../../interfaces/checkoutInterface";
import { UseFormReturnType } from "@mantine/form";

interface IAdressFormProps {
  flexAdressLines?: boolean;
  checkoutFormTemplate: UseFormReturnType<INewCheckout, (values: INewCheckout) => INewCheckout>
  // checkoutTemplate: UseFormReturnType<ICheckout | IOrderCheckout>;
}

export default function AdressInfoForm({ flexAdressLines, checkoutFormTemplate }: IAdressFormProps) {

  return (
    <div>
      <div
        style={{
          display: flexAdressLines ? "flex" : "block",
          gap: ".8rem",
        }}
      >
        <TextInput
          {...checkoutFormTemplate.getInputProps("customer.address.line1")}
          required
          w="100%"
          size="sm"
          label="Adresa linie 1"
        />
        <TextInput
          {...checkoutFormTemplate.getInputProps("customer.address.line2")}
          w="100%"
          size="sm"
          label="Adresa linie 2"
        />
      </div>
      <TextInput
        {...checkoutFormTemplate.getInputProps("customer.address.city")}
        w="100%"
        size="sm"
        label="Oras"
      />
      <TextInput
        {...checkoutFormTemplate.getInputProps("customer.address.county")}
        description="Atentie! Operam doar in unele judete!"
        w="100%"
        size="sm"
        label="Judet"
      />
      <TextInput
          {...checkoutFormTemplate.getInputProps("customer.address.postalCode")}
        required
        w="100%"
        size="sm"
        label="Cod postal"
      />
    </div>
  );
}
