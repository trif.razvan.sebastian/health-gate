import { ICheckoutLineItems } from "./cartInterface";

export interface ICheckout {
  fullName: string;
  email: string;
  phoneNumber: string[] | string;
  adressData: {
    primaryAdress: string;
    secondaryAdress?: string;
    postalCode: string;
  };
  paymentType: string;
  cardData: {
    cardOwner: string;
    cardNumber: string;
    cardExpDate: {
      cardExpMonth: string;
      cardExpYear: string;
    };
    cardCvv: string;
  };
}

export interface IOrderCheckout extends ICheckout {
  cart: [];
  checkoutAt: string;
}

export enum OrderStatus {
  PROCESSING = 'PROCESSING',
  COOKING = 'COOKING',
  SHIPPING = 'SHIPPING',
  DELIVERED = 'DELIVERED',
  CANCELED = 'CANCELED',
}

export enum PaymentStatus {
  PROCESSING_PAYMENT = 'PROCESSING_PAYMENT',
  PAID = 'PAID',
  UNPAID = 'UNPAID',
  CANCELED = 'CANCELED',
}

export enum PaymentMethod {
  CASH = 'CASH',
  CARD = 'CARD',
}

export interface INewCheckout {
  id?: string;
  orderId: number;
  customer: {
    customerId: string;
    fullName: string;
    email: string;
    phone: string;
    address: {
      line1: string;
      line2: string;
      city: string;
      county: string;
      postalCode: string;
    };
  };
  items: ICheckoutLineItems[];
  subtotal: number;
  tax: number;
  total: number;
  shipping: number;
  discount: number;
  payment: {
    method: PaymentMethod;
    paymentStatus: PaymentStatus;
  };
  status: OrderStatus;
  createdAt: string;
  updatedAt: string;
  sessionId?: string; //only if paying with card
  tags?: string[]
}
