export interface IMenu {
  id?: string;
  title: string;
  description: string;
  price: number;
  weight: number;
  nutritionalValues: {
    calories: string;
    carbohydrates: string;
    proteins: string;
    fats: string;
    fibers: string;
  };
  image: string;
  category: string;
}
