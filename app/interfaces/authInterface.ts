import { IUserSubscription } from "./addSubscriptionInterface";

export interface ISignUp {
  fullName: string;
  email: string;
  password?: string;
  confirmPassword?: string;
  termsAndConditionsChecked?: boolean;
}

export interface IAuth extends ISignUp {
  id?: string;
  phoneNumbers: string[];
  createdAt?: string;
  updatedAt?: string;
  orders?: number;
  ordersStreak?: number;
  activeSubscription: IUserSubscription;
  favorites: string[];
  lastMenuOrder: string;
}

export interface ILogin {
  email: string;
  password: string;
}
