import { ICheckout } from "./checkoutInterface";

export interface IAddSubscription {
  title: string;
  price: number;
  benefits: string[];
  code: string;
  plan: string;
  description?: string;
  rules: {
    label: string;
    interval: string;
    intervalCount: number;
  };
  discountId: string;
}

export interface ISubscription extends IAddSubscription {
  id: string;
  createdAt: string;
  discount: string;
}

export interface IUserSubscription {
  subscriptionId: string;
  subscriptionDiscountAmount?: number;
  isActive: boolean;
  period: {
    start: string;
    end: string;
    update: string
  };
  stripeSubscriptionId: string;
  cancelAtPeriodEnd: boolean;
  discountId: string;
}


export enum ESubscriptionActions {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
  DELETE = "DELETE"
}