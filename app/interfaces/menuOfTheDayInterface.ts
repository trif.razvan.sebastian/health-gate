export interface IAddMenuOfTheDay {
  menuItemOne: string;
  menuItemTwo: string;
  menuTotal: number;
  forDay: string;
  title?: string
}

export interface IMenuOfTheDay extends IAddMenuOfTheDay {
  id: string;
  createdAt: string;
  updatedAt: string;
}
