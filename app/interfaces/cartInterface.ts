
export interface ICart {
    id?: string;
    productId: string;
    productPrice: number;
    productQuantity: number;
    productTitle: string;
    productImage: string;
    menuItems?: ""[]
}

export interface ICheckoutLineItems {
    cartId: string;
    productId: string;
    productPrice: number;
    productQuantity: number;
    productCurrency: string;
    menuItems?: string[]
}

export interface ICartItems {
    price: number
    quantity: number
    title: string
    image: string
    id: string
    cartId: string
    menuItems: string[] | []

}