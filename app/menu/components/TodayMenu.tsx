import { Flex, Space, Tabs } from "@mantine/core";
import moment from "moment";
import NoWeekendDeliveryMessage from "../../components/NoWeekendDeliveryMessage";
import SectionTitle from "../../components/SectionTitle";
import useGetCart from "../../hooks/firebase/useGetCart";
import useGetMenuOfTheDay from "../../hooks/firebase/useGetMenuOfTheDay";
import useGetUtils from "../../hooks/firebase/useGetUtils";
import { IAuth } from "../../interfaces/authInterface";
import { IMenu } from "../../interfaces/menuInterface";
import classes from "../../modules/Menu.module.css";
import addToCart from "../../utils/addToCart";
import weekendButtonDisabled from "../../utils/weekendButtonDisabled";
import TodayMenuItems from "./TodayMenuItems";

interface ITodayMenu {
  menu: IMenu[];
  userId: string;
  userData: IAuth;
}

const days = [
  { label: "Lu", value: "monday" },
  { label: "Ma", value: "tuesday" },
  { label: "Mi", value: "wednesday" },
  { label: "Jo", value: "thursday" },
  { label: "Vi", value: "friday" },
];

export default function TodayMenu({ menu, userData, userId }: ITodayMenu) {
  const today = moment().format("dddd").toLowerCase();
  const { menuOfTheDayArr } = useGetMenuOfTheDay();
  const { realtimeCart } = useGetCart();
  const { utilsObj } = useGetUtils();
  const tabs = days.map((day, i) => (
    <Tabs.Tab
      className={classes.tab}
      disabled={
        i < days.findIndex((val) => val.value === today) ||
        weekendButtonDisabled()
      }
      key={i}
      value={day.value}
    >
      {day.label}
    </Tabs.Tab>
  ));

  return (
    <div>
      <SectionTitle text="Meniul zilei pentru azi" />
      <Space h={45} />
      <Tabs color="var(--primary-color)" defaultValue={today}>
        <Tabs.List mb="1.5rem" grow>
          {tabs}
        </Tabs.List>
        {days.map((day, i) => {
          const filteredTodayMenu = menuOfTheDayArr.find(
            (item) => item.forDay === day.value
          );
          const buttonCondition =
            realtimeCart.length &&
            realtimeCart
              ?.map((prod) => prod.productId)
              .includes(filteredTodayMenu?.id as string);

          const firstMenuProduct = menu.find(
            (item) => item.id === filteredTodayMenu?.menuItemOne
          );
          const secondMenuProduct = menu.find(
            (item) => item.id === filteredTodayMenu?.menuItemTwo
          );
          const econ =
            (filteredTodayMenu?.menuTotal as number) -
            ((firstMenuProduct?.price as number) +
              (secondMenuProduct?.price as number));

          return (
            <Tabs.Panel key={i} value={day.value}>
              <div className={classes.dayFlex}>
                <TodayMenuItems
                  title={filteredTodayMenu?.title as string}
                  firstProduct={firstMenuProduct}
                  secondProduct={secondMenuProduct}
                />
                <div>
                  <Flex justify="space-between" gap="5rem">
                    <p className="p-text">Preparate:</p>
                    <p className="p-text">
                      {(firstMenuProduct?.price as number) +
                        (secondMenuProduct?.price as number)}
                      lei
                    </p>
                  </Flex>
                  <Flex justify="space-between" gap="5rem">
                    <p className="p-text">Meniu:</p>
                    <p className="p-text">{filteredTodayMenu?.menuTotal} lei</p>
                  </Flex>
                  <Flex justify="space-between" gap="5rem">
                    <p className="p-text">Economisesti:</p>
                    <p className="p-text">{econ} lei</p>
                  </Flex>
                  <Flex justify="space-between" gap="5rem">
                    <p style={{ fontSize: "1.25rem" }} className="p-text-dark">
                      TOTAL:
                    </p>
                    <p style={{ fontSize: "1.25rem" }} className="p-text-dark">
                      {filteredTodayMenu?.menuTotal} lei
                    </p>
                  </Flex>
                  <button
                    disabled={filteredTodayMenu?.forDay !== today}
                    onClick={() =>
                      addToCart({
                        realtimeCart,
                        userId,
                        price: filteredTodayMenu?.menuTotal as number,
                        image: utilsObj?.menuImage as string,
                        menuItems: [
                          filteredTodayMenu?.menuItemOne,
                          filteredTodayMenu?.menuItemTwo,
                        ],
                        title: filteredTodayMenu?.title,
                        productId: filteredTodayMenu?.id as string,
                        isActive: userData?.activeSubscription
                          .isActive as boolean,
                        lastMenuOrder: userData?.lastMenuOrder as string,
                      })
                    }
                    className={
                      filteredTodayMenu?.forDay !== today
                        ? "primary-button-disabled-full"
                        : buttonCondition
                        ? "primary-button-dark-full"
                        : "secondary-button-dark-full"
                    }
                  >
                    {buttonCondition ? "In cos | +1" : "Adauga in cos"}
                  </button>
                </div>
              </div>
            </Tabs.Panel>
          );
        })}
      </Tabs>
      {weekendButtonDisabled() && <NoWeekendDeliveryMessage mt="1.5rem" />}
    </div>
  );
}
