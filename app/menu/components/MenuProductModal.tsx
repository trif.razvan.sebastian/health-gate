import { Flex } from "@mantine/core";
import EqualGapContainer from "../../components/EqualGapContainer";
import useGetCart from "../../hooks/firebase/useGetCart";
import useGetUserData from "../../hooks/firebase/useGetUserData";
import { IMenu } from "../../interfaces/menuInterface";
import classes from "../../modules/Menu.module.css";
import addToCart from "../../utils/addToCart";
import AddToFavoritesButton from "./AddToFavoritesButton";
import { IAuth } from "../../interfaces/authInterface";

interface IMenuProductModal {
  openedProduct?: IMenu;
  userData: IAuth;
  userId: string
}

export default function MenuProductModal({ openedProduct, userData, userId }: IMenuProductModal) {
  const { realtimeCart } = useGetCart();

  const buttonCondition =
    realtimeCart.length &&
    realtimeCart
      ?.map((prod) => prod.productId)
      .includes(openedProduct?.id as string);

  const displayMeasureUnit = () => {
    if (
      openedProduct?.category === "soup" ||
      openedProduct?.category === "juice"
    ) {
      return "ml";
    } else {
      return "g";
    }
  };

  const nutritionalValuesArray = [
    {
      value: openedProduct?.nutritionalValues.calories,
      label: "Calorii",
    },
    {
      value: openedProduct?.nutritionalValues.carbohydrates,
      label: "Carbohidrati",
    },
    {
      value: openedProduct?.nutritionalValues.fats,
      label: "Grasimi",
    },
    {
      value: openedProduct?.nutritionalValues.proteins,
      label: "Proteine",
    },
    {
      value: openedProduct?.nutritionalValues.fibers,
      label: "Fibre",
    },
  ];

  return (
    <div className={classes.modalContainer}>
      <EqualGapContainer gap="2rem">
        <Flex pos="relative" w="100%" align="flex-end" gap="1rem" justify="space-between">
          <div className={classes.floatingFavoriteButtonContainerModal}>
              <AddToFavoritesButton
                openProductId={openedProduct?.id as string}
              />
          </div>
          <img
            className={classes.modalImage}
            src={openedProduct?.image}
            alt=""
          />
          <div style={{ width: "100%" }}>
            <EqualGapContainer gap="8px">
              {/* <Flex justify="space-between" align="center"> */}
              <h4 className="h4-text-light">{openedProduct?.title}</h4>
              {/* <AddToFavoritesButton
                  openProductId={openedProduct?.id as string}
                /> */}
              {/* </Flex> */}
              <Flex justify="flex-end" align="center">
                {/* <p>counter</p> */}
                <h1 style={{ fontWeight: 600 }} className="h1-text">
                  {openedProduct?.price} lei
                </h1>
              </Flex>
              <button
                onClick={(event) =>
                  addToCart({
                    price: openedProduct?.price as number,
                    productId: openedProduct?.id,
                    userId,
                    image: openedProduct?.image,
                    title: openedProduct?.title,
                    realtimeCart,
                    userData,
                    isActive: userData?.activeSubscription.isActive as boolean,
                    lastMenuOrder: userData?.lastMenuOrder as string,
                    event,
                  })
                }
                className={
                  buttonCondition
                    ? "primary-button-dark-full"
                    : "secondary-button-dark-full"
                }
              >
                {buttonCondition ? "In cos | +1" : "Adauga in cos"}
              </button>
            </EqualGapContainer>
          </div>
        </Flex>
        <EqualGapContainer gap="1rem">
          <h3 className="h3-text-dark">Descriere</h3>
          <p className="p-text">{openedProduct?.description}</p>
        </EqualGapContainer>
        <EqualGapContainer gap="1rem">
          <Flex justify="space-between" align="flex-end">
            <h3 className="h3-text-dark">Valori nutritionale</h3>
            <p className="p-text-dark">
              Per {openedProduct?.weight}
              {displayMeasureUnit()}
            </p>
          </Flex>
          <div>
            {nutritionalValuesArray.map((val, i) => (
              <Flex
                key={i}
                mb={8}
                justify="space-between"
                style={{ borderBottom: "1px solid rgba(117, 117, 117, 50%)" }}
              >
                <p className="p-text">{val.label}</p>
                <p className="p-text-dark">{val.value}</p>
              </Flex>
            ))}
          </div>
        </EqualGapContainer>
      </EqualGapContainer>
    </div>
  );
}
