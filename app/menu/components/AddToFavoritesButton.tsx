import { ActionIcon } from "@mantine/core";
import FavoriteRoundedIcon from "@mui/icons-material/FavoriteRounded";
import useGetUserData from "../../hooks/firebase/useGetUserData";
import addToFavorites from "../../utils/addToFavorites";
import removeFromFavorites from "../../utils/removeFromFavorites";

interface IAddToFavoritesButton {
  openProductId: string;
}

export default function AddToFavoritesButton({
  openProductId,
}: IAddToFavoritesButton) {
  const { userData, userId } = useGetUserData();
  return userData?.favorites.includes(openProductId) ? (
    <ActionIcon
      onClick={() =>
        removeFromFavorites(
          userId,
          openProductId,
          userData?.favorites as string[]
        )
      }
      size="lg"
      variant="transparent"
    >
      <FavoriteRoundedIcon
        style={{
          color: "var(--primary-color)",
        }}
        className="icon"
      />
    </ActionIcon>
  ) : (
    <ActionIcon
      onClick={() =>
        addToFavorites(userId, openProductId, userData?.favorites as string[])
      }
      size="lg"
      variant="transparent"
    >
      <FavoriteRoundedIcon
        style={{
          color: "var(--footer-color)",
        }}
        className="icon"
      />
    </ActionIcon>
  );
}
