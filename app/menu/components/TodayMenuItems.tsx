import { Badge, Flex, Space, Tooltip } from "@mantine/core";
import { IMenu } from "../../interfaces/menuInterface";
import classes from "../../modules/Menu.module.css";
import { categoryThumbnails } from "../../components/home/CategoriesHighlights";

interface ITodayMenuItems {
  title: string;
  firstProduct?: IMenu;
  secondProduct?: IMenu;
}

export default function TodayMenuItems({
  firstProduct,
  secondProduct,
  title,
}: ITodayMenuItems) {
  
  const productCard = (prod: IMenu) => {
    const { image, title, price } = prod ?? {};
    const category = categoryThumbnails.find(
      (item) => item.value === prod?.category
    );
    return (
      <div className={classes.itemFlex}>
        <img className={classes.itemImage} src={image} alt={title} />
        <div>
          <Flex gap="1.5rem" mb={8}>
            <Tooltip label={title}>
              <p className="p-text-truncate">{title}</p>
            </Tooltip>
            <h4 style={{ color: "var(--text-color)" }} className="h4-text">
              {price} lei
            </h4>
          </Flex>
          <Badge color="var(--light-blue)" c="var(--primary-color)">
            {category?.label}
          </Badge>
        </div>
      </div>
    );
  };

  return (
    <div>
      <h3 className="h3-text-dark">{title}</h3>
      <Space h={8} />
      <div className={classes.itemsFlexContainer}>
        {productCard(firstProduct as IMenu)}
        <h3 className="h3-text-dark">+</h3>
        {productCard(secondProduct as IMenu)}
      </div>
    </div>
  );
}
