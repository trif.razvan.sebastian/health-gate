import { BackgroundImage, Flex } from "@mantine/core";
import { categoryThumbnails } from "../../components/home/CategoriesHighlights";

interface ICategoryThumbnail {
  category: string;
}

export default function CategoryThumbnail({ category }: ICategoryThumbnail) {
  const selectedCategory = categoryThumbnails.find((item) => {
    if (category) return item.value === category;
    else return item.value === "main-course";
  });

  return (
    <BackgroundImage h="32rem" src={selectedCategory?.image as string}>
      <Flex
        bg="var(--dark-transparent)"
        justify="center"
        align="center"
        h="32rem"
      >
        <h2 className="h3-text-light">{selectedCategory?.label}</h2>
      </Flex>
    </BackgroundImage>
  );
}
