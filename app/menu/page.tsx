"use client";

import { Loader, Modal, Space, Tabs, Text } from "@mantine/core";
import { useDisclosure } from "@mantine/hooks";
import { useRouter, useSearchParams } from "next/navigation";
import { useEffect } from "react";
import EqualGapContainer from "../components/EqualGapContainer";
import SectionTitle from "../components/SectionTitle";
import MenuHighlightsItemCard from "../components/home/MenuHighlightsItemCard";
import useGetCart from "../hooks/firebase/useGetCart";
import useGetMenu from "../hooks/firebase/useGetMenu";
import useGetUserData from "../hooks/firebase/useGetUserData";
import { IAuth } from "../interfaces/authInterface";
import classes from "../modules/Menu.module.css";
import { goToCategory } from "../utils/goToCategory";
import openProduct from "../utils/openProduct";
import CategoryThumbnail from "./components/CategoryThumbnail";
import MenuProductModal from "./components/MenuProductModal";
import TodayMenu from "./components/TodayMenu";

const menuCategories = [
  { label: "Felul principal", value: "main-course" },
  { label: "Supe", value: "soup" },
  { label: "Salate", value: "salad" },
  { label: "Deserturi", value: "dessert" },
  { label: "Sucuri naturale", value: "juice" },
  { label: "Gustari", value: "snack" },
];

export default function Menu() {
  const searchParams = useSearchParams();
  const router = useRouter();
  const activeCategory = searchParams.get("category") || "main-course";
  const [opened, { open, close }] = useDisclosure(false);
  const { menu } = useGetMenu();
  const { realtimeCart } = useGetCart();
  const { userData, userId } = useGetUserData();
  const openedProductId = searchParams.get("product");
  const openedProduct = menu.find((prod) => prod.id === openedProductId);

  const openProductModal = (id: string) => {
    openProduct(id, router, searchParams);
    open();
  };

  const closeModal = () => {
    openProduct(null, router, searchParams);
    close();
  };

  useEffect(() => {
    if (openedProductId) {
      open();
    }
  }, [openedProductId]);

  const menuTabs = menuCategories.map((category, i) => (
    <Tabs.Tab
      onClick={() => goToCategory(category.value, router, searchParams)}
      className={classes.tab}
      key={i}
      value={category.value}
    >
      {category.label}
    </Tabs.Tab>
  ));

  const menuPannels = menuCategories.map((category, i) => {
    const sameCategoryItems = menu.filter(
      (item) => item.category === category.value
    );
    return (
      <Tabs.Panel key={i} value={category.value}>
        {sameCategoryItems.length ? (
          <div className={classes.menuGrid}>
            {sameCategoryItems?.map((item, i) => (
              <MenuHighlightsItemCard
                openProductModal={openProductModal}
                key={i}
                item={item}
                realtimeCart={realtimeCart}
                userData={userData as IAuth}
                userId={userId}
              />
            ))}
          </div>
        ) : (
          <Text ta="center">Nu se gasesc produse...</Text>
        )}
      </Tabs.Panel>
    );
  });

  return menu.length ? (
    <div className="margin-container">
      <Modal
        radius={10}
        withCloseButton={false}
        size="40rem"
        opened={opened}
        onClose={closeModal}
      >
        <MenuProductModal
          userData={userData as IAuth}
          userId={userId}
          openedProduct={openedProduct}
        />
      </Modal>
      <EqualGapContainer gap="6rem">
        <CategoryThumbnail category={activeCategory as string} />
        <TodayMenu menu={menu} userData={userData as IAuth} userId={userId} />
        <div>
          <SectionTitle text="Preparatele din meniu" />
          <Space h={45} />
          <Tabs color="var(--primary-color)" defaultValue={activeCategory}>
            <Tabs.List mb="1.5rem" grow>
              {menuTabs}
            </Tabs.List>
            {menuPannels}
          </Tabs>
        </div>
      </EqualGapContainer>
    </div>
  ) : (
    <Loader size="lg" color="var(--primary-color)" />
  );
}
