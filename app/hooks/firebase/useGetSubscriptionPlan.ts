import { useEffect, useState } from "react";
import { ISubscription } from "../../interfaces/addSubscriptionInterface";
import { IAuth } from "../../interfaces/authInterface";

interface IuseGetSubscriptionPlan {
  subscriptions: ISubscription[];
  userData?: IAuth;
}

export default function useGetSubscriptionPlan({
  subscriptions,
  userData,
}: IuseGetSubscriptionPlan) {
  const [subscriptionPlan, setSubscriptionPlan] = useState("FREE");

  useEffect(() => {
    if (subscriptions.length && userData?.activeSubscription.subscriptionId) {
      const tempSubPlan = subscriptions
        .find((sub) => sub.id === userData?.activeSubscription.subscriptionId)
        ?.plan.toUpperCase();

      setSubscriptionPlan(tempSubPlan as string);
    }
  }, [userData, subscriptions.length]);


  return {subscriptionPlan}
}
