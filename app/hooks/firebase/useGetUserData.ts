"use client";
import { onAuthStateChanged } from "firebase/auth";
import {
  Unsubscribe,
  collection,
  onSnapshot,
  query,
  where,
} from "firebase/firestore";
import { useEffect, useState } from "react";
import { auth, db } from "../../firebase/firebase";
import { IAuth } from "../../interfaces/authInterface";

export default function useGetUserData() {
  const [email, setEmail] = useState<string | null>(null);
  const [userData, setUserData] = useState<undefined | IAuth>(undefined);
  const [userId, setUserId] = useState("");

  useEffect(() => {
    let tempEmail: string | null = "";
    const getUserEmail = () => {
      onAuthStateChanged(auth, async (user) => {
        if (user) {
          setEmail(user.email);
        } else {
          return;
        }
      });
    };

    getUserEmail();
    // console.log("got the user email");
  }, []);

  let unsubscribe: Unsubscribe | null | void = null;

  const getUserDoc = async () => {
    if(!email) return;
    try {
      const queryUser = query(
        collection(db, "users"),
        where("email", "==", email)
      );
      unsubscribe = onSnapshot(queryUser, (querySnapshot) => {
        querySnapshot.forEach((user) => {
          // @ts-ignore
          setUserData({ ...user.data(), id: user.id });
          setUserId(user.id);
          // console.log("Got user data");
        });
      });
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
  };

  useEffect(() => {
    getUserDoc();
    if (unsubscribe) {
      // @ts-ignore
      () => unsubscribe();
    }
    // console.log("got the user with email", email);
  }, [email]);

  return { userData, getUserDoc, userId };
}
