
import { doc, getDoc } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import { IUtilsObj } from "../../interfaces/utilsObjInterface";

export default function useGetUtils() {
  const [utilsObj, setUtilsObj] = useState<IUtilsObj | undefined>(undefined);

  useEffect(() => {
    const getUtils = async () => {
      const docRef = doc(db, "utils", process.env.NEXT_PUBLIC_UTILS_DOC!);
      const docSnap = await getDoc(docRef);
      if (docSnap.exists()) {
        // @ts-ignore
        setUtilsObj(docSnap.data());
      } else console.log("Utils doc doesn't exist");
    };
    getUtils();
  }, []);

  return { utilsObj };
}
