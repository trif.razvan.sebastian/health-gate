import { collection, getDocs, onSnapshot, query } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import useGetUserData from "./useGetUserData";
import { ICart } from "../../interfaces/cartInterface";
import { Unsubscribe } from "firebase/auth";

export default function useGetCart() {
  const [cart, setCart] = useState<ICart[] | []>([]);
  const [realtimeCart, setRealtimeCart] = useState<ICart[] | []>([]);

  const { userId } = useGetUserData();

  const getCart = async () => {
    const tempCart: ICart | [] = [];
    try {
      const querySnapshot = await getDocs(
        collection(db, `users/${userId}/cart`)
      );
      querySnapshot.forEach((doc) => {
        // @ts-ignore
        tempCart.push({ ...doc.data(), id: doc.id });
      });
      setCart(tempCart);
    } catch (error) {
      // @ts-ignore
      console.log(error.message);
    }
  };

  let unsubscribe: Unsubscribe | null | void = null;

  const getRealtimeCart = async () => {
    const tempCart: ICart[] = [];
    const q = query(collection(db, `/users/${userId}/cart`));
    unsubscribe = onSnapshot(q, (querySnapshot) => {
      // querySnapshot.forEach((doc) => {
      //   // @ts-ignore
      //   tempCart.push({ ...doc.data(), id: doc.id });
      //   setRealtimeCart(tempCart);
      // });
      // @ts-ignore
      const updatedCart: ICart[] = querySnapshot.docs.map((doc) => ({
        ...doc.data(),
        id: doc.id,
      }));
      setRealtimeCart(updatedCart);
    });
  };

  useEffect(() => {
    if (userId) {
      getRealtimeCart();
      // console.log("got the realtime cart");
    }
    return () => {
      if (unsubscribe) {
        // Invoke the unsubscribe function
        unsubscribe();
      }
    };
  }, [userId]);

  // useEffect(() => {
  //   if (userId) {
  //     getCart();
  //     // console.log("got the cart");
  //   }
  // }, [userId]);

  return { cart, realtimeCart };
}
