import { collection, getDocs, orderBy, query, where } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import { INewCheckout } from "../../interfaces/checkoutInterface";

export default function useGetOrders(id: string) {
  const [orders, setOrders] = useState<INewCheckout[]>([]);

  useEffect(() => {
    const getOrders = async (customerId: string) => {
      const tempOrders: INewCheckout[] = [];
      try {
        const q = query(
          collection(db, "orders"),
          where("customer.customerId", "==", customerId),
          orderBy("createdAt", "desc")
        );
        const querySnapshot = await getDocs(q);
        querySnapshot.forEach((doc) => {
          // @ts-ignore
          tempOrders.push({ ...doc.data(), id: doc.id });
        });
        setOrders(tempOrders);
      } catch (error) {
        // @ts-ignore
        console.log(error.message);
      }
    };

    if (id) {
      getOrders(id);
    }
  }, [id]);

  return { orders };
}
