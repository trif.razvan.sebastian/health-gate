import { collection, getDocs, query } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import { ISubscription } from "../../interfaces/addSubscriptionInterface";

export default function useGetSubscriptions() {
  const [subscriptions, setSubscriptions] = useState<ISubscription[]>([]);

  useEffect(() => {
    const getSubscriptions = async () => {
      const tempSubscriptions: ISubscription[] = [];
      try {
        const querySubscriptions = query(collection(db, "subscriptions"));

        const querySnapshot = await getDocs(querySubscriptions);

        querySnapshot.forEach((doc) => {
          // @ts-ignore
          tempSubscriptions.push({ ...doc.data(), id: doc.id });
        });
        setSubscriptions(tempSubscriptions);
        // console.log("got the subscriptions");
      } catch (error) {
        // @ts-ignore
        console.log(error.message);
      }
    };

    getSubscriptions();
    // console.log("getting subs");
  }, []);

  return { subscriptions };
}
