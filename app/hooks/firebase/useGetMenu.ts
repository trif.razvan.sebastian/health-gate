import { collection, getDocs, query } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import { IMenu } from "../../interfaces/menuInterface";

const idsToDsiplay = [
  "CGk2lioo1DM0u8EcS4OA",
  "hR2ZsvCEO60Y8NY74Mi7",
  "epgTbRCa7uvEsN61qzWO",
  "jB2YZAuw22s6SLVzYaD2",
  "gJHLuakcfIDBmWeXwBT5",
  "naaTqkpxdOhNDA4fAlLS",
  "IPQfEJxjUe5pTXXEcU4E",
  "08rxSzhxXQqqMOPdSZMy",
];

export default function useGetMenu() {
  const [menu, setMenu] = useState<IMenu[]>([]);
  const [displayMenu, setDisplayMenu] = useState<IMenu[]>([]);

  useEffect(() => {
    const getMenu = async () => {
      const tempMenu: IMenu[] = [];
      const queryMenu = query(collection(db, "menu"));
      const querySnapshot = await getDocs(queryMenu);
      querySnapshot.forEach((doc) => {
        // @ts-ignore
        tempMenu.push({ ...doc.data(), id: doc.id });
      });
      setMenu(tempMenu);
    };

    getMenu();
    // console.log("got the menu");
  }, []);

  useEffect(() => {
    if (menu.length) {
      const tempDisplayMenu = menu.filter((item) =>
        idsToDsiplay.includes(item.id as string)
      );
      setDisplayMenu(tempDisplayMenu);
    }
  }, [menu]);

  return { menu, displayMenu };
}
