import { collection, getDocs, query } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../firebase/firebase";
import { IMenuOfTheDay } from "../../interfaces/menuOfTheDayInterface";

export default function useGetMenuOfTheDay() {
  const [menuOfTheDayArr, setMenuOfTheDayArr] = useState<IMenuOfTheDay[]>([]);

  useEffect(() => {
    const getMenuOfTheDay = async () => {
      const tempMenu: IMenuOfTheDay[] = [];
      const queryMenu = query(collection(db, "menuOfTheDay"));
      const querySnapshot = await getDocs(queryMenu);
      querySnapshot.forEach((doc) => {
        // @ts-ignore
        tempMenu.push({ ...doc.data(), id: doc.id });
      });
      setMenuOfTheDayArr(tempMenu);
    };

    getMenuOfTheDay();
    // console.log("got the menu of the day");
  }, []);

  return { menuOfTheDayArr };
}
