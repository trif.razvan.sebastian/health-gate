import { useState } from "react";
import profileLetterIconData from "../utils/profileLetterIconData.json" 
const useGetProfileLettersData = () => {
  const [profileLettersArr, setProfileLettersArr] = useState(profileLetterIconData);

  return { profileLettersArr };
};

export default useGetProfileLettersData;
