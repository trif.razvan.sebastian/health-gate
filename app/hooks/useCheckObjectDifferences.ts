import { useEffect, useState } from "react";
import { IAuth } from "../interfaces/authInterface";
import { UseFormReturnType } from "@mantine/form";

interface ICheckDifferencesProps {
  rest: IAuth;
  userData?: IAuth;
  authFormTemplate: UseFormReturnType<IAuth>;
}

export default function useCheckObjectDifferences({
  rest,
  userData,
  authFormTemplate,
}: ICheckDifferencesProps) {
  const [isSubmitDisabled, setisSubmitDisabled] = useState(true);

  useEffect(() => {
    if (userData) {
      setisSubmitDisabled(
        Object.entries(rest).sort().toString() ===
          Object.entries(userData as {})
            .sort()
            .toString()
      );
    }
  }, [userData, authFormTemplate]);

  return { isSubmitDisabled };
}
